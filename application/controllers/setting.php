<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index($setting_value=1)
	{
		$bg = 'style="background: url('.base_url().'assets/images/'.strtolower($setting_value).'.jpg) repeat scroll 0% 0% transparent;"';
		$this->session->set_userdata('background_setting',$bg);
		redirect('/');
	}
}
