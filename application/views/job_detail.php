<?php $this->load->view('header'); ?>

<body <?php echo $background_setting; ?>>
	<div id="wrapper" class="container_16">
		
		<?php $this->load->view('menu'); ?>
		<?php $this->load->view('side_bar'); ?>
		
		<div id="main" class="grid_13 omega jobs_list">
			<div class="content round_all clearfix">
					<div class="clearfix">
						<div style="float:left;width:40%">
							<div>
								<h3 style="border-bottom:1px solid #6E7785">Advertiser : <span class="red"><?php echo $detail->advertisername; ?></span></h3>
							</div>
							
							<div style="position:relative;margin:30px 0;min-height:500px">
								<h3 style="margin-bottom:15px;" class="red"><?php echo $detail->position; ?></h3>
								
								<p><span class="grey">Country: </span><?php echo $detail->country; ?></p>
								<p><span class="grey">Location: </span><?php echo $detail->location . '&raquo;' . $detail->area; ?></p>
								<p><span class="grey">Work Type: </span><?php echo $detail->employmenttype; ?></p>
								<p><span class="grey">Advertiser: </span><?php echo $detail->advertisername; ?></p>
								
								<div style="position:absolute;bottom:0">
									<a class="button_example" href="<?php echo base_url(); ?>apply/<?php echo protect_url($detail->PrimaryKey); ?>">Apply Here</a>
									<div style="padding:10px 0;text-align:center">
										<a href="<?php echo base_url(); ?>assets/share.php?url=<?php echo base_url() . 'detail/' . protect_url($detail->PrimaryKey);?>" onclick="javascript:window.open('<?php echo base_url(); ?>assets/share.php?url='+encodeURIComponent('<?php echo base_url(); ?>detail/<?php echo protect_url($detail->PrimaryKey); ?>')+'&title=<?php echo $detail->position; ?>','sharer','toolbar=0,status=0,width=600,height=700');return false;" style="text-decoration:none">
											<img src="<?php echo base_url(); ?>assets/images/share/email.png" alt="Email">
										</a>	
										
										<a href="http://www.facebook.com/share.php?u=<?php echo base_url() . 'detail/' . protect_url($detail->PrimaryKey);?>" 
											onclick="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]='+encodeURIComponent('<?php echo base_url(); ?>detail/<?php echo protect_url($detail->PrimaryKey); ?>')+'&p[title]=<?php echo urlencode($detail->position)?>&p[summary]=<?php echo urlencode(strip_tags($detail->description))?>&p[images][0]=<?php echo base_url(); ?>assets/images/apply.png','sharer','toolbar=0,status=0,width=620,height=436');return false;" style="text-decoration:none">
											<img src="<?php echo base_url(); ?>assets/images/share/facebook.png" alt="Facebook">
										</a>
										<a href="https://twitter.com/share?url=<?php echo base_url() . 'detail/'. protect_url($detail->PrimaryKey);?>" onclick="javascript:window.open('https://twitter.com/share?url='+encodeURIComponent('<?php echo base_url(); ?>detail/<?php echo protect_url($detail->PrimaryKey)?>')+'&text=<?php echo $detail->position; ?>','sharer','toolbar=0,status=0,width=620,height=436');return false;" style="text-decoration:none">
											<img src="<?php echo base_url(); ?>assets/images/share/twitter.png" alt="Twitter">
										</a>
										
										<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url() . urlencode('detail/'.protect_url($detail->PrimaryKey));?>" onclick="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?php echo base_url(); ?>detail/<?php echo protect_url($detail->PrimaryKey); ?>')+'&title=<?php echo $detail->position; ?>','sharer','toolbar=0,status=0,width=620,height=436');return false;" style="text-decoration:none">
											<img src="<?php echo base_url(); ?>assets/images/share/linkedin.png" alt="Linkedin">
										</a>
										
										<a href="https://plus.google.com/share?url=<?php echo base_url() . urlencode('detail/'.protect_url($detail->PrimaryKey));?>" onclick="javascript:window.open('https://plus.google.com/share?url='+encodeURIComponent('<?php echo base_url()?>detail/<?php echo protect_url($detail->PrimaryKey); ?>'),'sharer','toolbar=0,status=0,width=620,height=436');return false;" style="text-decoration:none">
											<img src="<?php echo base_url(); ?>assets/images/share/googleplus.png" alt="Google+">	
										</a>
									</div>
								</div>
							</div>
							
						</div>
						<div style="float:right;width:56%">
							<div>
								<div class="clearfix" style="border-bottom:1px solid #6E7785;">
									<h6 style="float:left"><a class="red" style="text-decoration:none;font-size:11px;" href="<?php echo base_url(); ?>advertiser/search/<?php echo protect_url($detail->advertisername); ?>">More jobs by this advertiser</a></h6>
									<h6 style="float:right"><a class="red" style="text-decoration:none;font-size:11px;" href="javascript:void(0)" onclick="window.history.go(-1)">&laquo; back to results</a></h6>
								</div>
								<div style="margin:30px 0">
									<p><?php echo $detail->description; ?></p>
								</div>
							</div>
						</div>
					</div>
					
			</div>
			
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
	
</body>
</html>