<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sector extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index($sector_id=NULL)
	{
		
		$type_array = array('any','permanent','contract');
		$type_j = implode(',',$type_array);
		
		$options=array();
		
		
		
		$data['header_search_title'] = 'job(s) in <b>All Sectors</b>';
		
		if(isset($sector_id)){
			$dec_sector_id = $this->dec($sector_id);
			
			if($dec_sector_id != 'all'){
				$this->Mymodel->set_table('xml_jobg8_oz');
				$get_name_cat = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => $dec_sector_id));
				$data['header_search_title'] = $get_name_cat->classification . ' job(s) in All Sectors';
			}
			
		}
		
		if(isset($_GET['search'])){
			$options['key'] = 'location';
			$options['value'] = $_GET['search'];
			
			if(isset($sector_id) &&  $this->dec($sector_id) != 'all'){
				$dec_sector_id = $this->dec($sector_id);
				
				$this->Mymodel->set_table('xml_jobg8_oz');
				$get_name_cat = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => $dec_sector_id));
				
				$data['header_search_title'] = $get_name_cat->classification . ' job(s) in <b>'. ucwords($_GET['search']) .'</b>';
			} else {
				$data['header_search_title'] = ' all job(s) categories in <b>' . ucwords($_GET['search']) . '</b>';
			}
			
		}
		
		if(isset($_GET['category'])){
			$options['key'] = 'classification_valueid';
			$options['value'] = urldecode($_GET['category']);
			
			$this->Mymodel->set_table('xml_jobg8_oz');
			$get_name_cat = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => urldecode($_GET['category'])));
			$data['header_search_title'] = $get_name_cat->classification . ' jobs in All Sectors';
		}
		
		$data['left_side_categories_jobs'] = array();
		$is_all = (isset($setor_id)) ? false : true;
		if($is_all){
		
			$all_categories = $this->db->query("
				SELECT DISTINCT(classification),classification_valueid FROM xml_jobg8_oz
			")->result();
			foreach($all_categories as $a){
				
				$res = $this->db->query("SELECT COUNT(*) AS total FROM xml_jobg8_oz WHERE classification_valueid = $a->classification_valueid")->row();
				$a->total = $res->total;
			}
			
			$data['left_side_categories_jobs'] = $all_categories;
		}
		$data['show_left_side_categories'] = $is_all;
		
		
		$sector_id = (isset($sector_id)) ? $this->dec($sector_id) : NULL;
		$sector_id = (isset($sector_id)) ? $sector_id : 'all';
		$this->session->set_userdata('sector_search',$sector_id);
		$data['all'] = $this->Mymodel->getJobsListBySector($sector_id,NULL,NULL,$options);
		
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'sector/'.$this->enc($sector_id).'/';
		$config['total_rows'] = count($data['all']);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$data = $this->_menu_data($data);
		
		$data['enable_nav_1'] = false;
		$data['enable_nav_2'] = true;
		$data['enable_nav_3'] = true;
		
		$data['enable_location_nav'] = false;
		
		$dist_nav = array();
		$inside_loop = array();
		if($sector_id != 'all'){
			$dist_nav = array('key' => 'classification_valueid', 'value' => $sector_id);
			$inside_loop = array('classification_valueid' => $sector_id);
		}
		
		$smart_nav = $this->Mymodel->distinctNav($dist_nav, 'location');
		foreach($smart_nav as $nav){
			$nav->total = sizeof($this->Mymodel->fetch_rows(NULL,array_merge($inside_loop, array('location' => $nav->location))));
		}
		
		$data['smart_nav_1'] = $smart_nav;
		
		$data['jobs'] = $this->Mymodel->getJobsListBySector($sector_id,ITEM_PER_PAGE,$this->uri->segment(3),$options);
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$data['current'] = 'sector_search';
		$data['current_key'] = 'classification_valueid';
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => $sector_id));
		
		$data['category'] = (($sector_id) != 'all') ? $row->classification : 'All Sectors';
		
		$this->load->view('jobs_list',$data);
		
	}
	
}



