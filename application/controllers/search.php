<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index($page=0)
	{
		
		if(!isset($_GET['keyword'])){
			redirect('/');
		}
		
		$keyword = urldecode($_GET['keyword']);
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$alljobs = $this->Mymodel->wildcard($keyword,NULL,NULL);
		
		$data['back'] = $this->session->userdata('uri_string');
		$data = $this->_menu_data($data);
		$data['all'] = $alljobs;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'search/';
		$config['total_rows'] = count($alljobs);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['uri_segment'] = 2;
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$jobs = $this->Mymodel->wildcard($keyword,ITEM_PER_PAGE,$this->uri->segment(2));
		$data['jobs'] = $jobs;
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		
		
		
		$this->load->view('jobs_list',$data);
		
	}
}



