<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advertiser extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}
	
	public function search($id=NULL,$page=0)
	{
		
		if(!isset($id)){
			redirect('/');
		}
		
		$id = $this->dec($id);
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$rows = $this->Mymodel->fetch_rows(NULL,array('advertisername' => $id));
		
		
		
		if(empty($rows)){
			redirect('/');
		}
		
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'advertiser/search/'.$this->enc($id).'/';
		$config['total_rows'] = sizeof($rows);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['uri_segment'] = '4';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$data['all'] = count($rows);
		$data['advertiser'] = $id;
		$data = $this->_menu_data($data);
		
		$data['jobs'] = $this->Mymodel->getJobsListByAdvertiser($id,ITEM_PER_PAGE,$page);
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$this->load->view('jobs_advertiser',$data);
	}
	
	
}
