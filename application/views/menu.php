		<?php $this->load->helper('protect_url'); ?>
		<div id="top_nav" class="nav_down bar_nav grid_16 round_all">	
			<ul class="round_all clearfix">
				<li>
					<a class="round_left" href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/admin_user.png">
						Oz Jobs Online
					</a>
				</li> 
				
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/Cog.png">
						Job Sector
						<span class="icon">&nbsp;</span>
					</a>
					<ul>
						<li><a href="<?php echo base_url(); ?>sector/" <?php echo (!$this->session->userdata('sector_search') || $this->session->userdata('sector_search') == 'all') ? 'class="red"' : ''; ?>>All Sectors</a></li>
						<?php foreach($job_sectors as $job_sector) : ?>
						<li><a <?php echo ($this->session->userdata('sector_search') == $job_sector->Classification1ID) ? 'class="red"' : '';?> href="<?php echo base_url(); ?>sector/<?php echo protect_url($job_sector->Classification1ID); ?>/"><?php echo $job_sector->Classification1Name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</li>
				
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/Books.png">
						Job Title
						<span class="icon">&nbsp;</span>
					</a>
					<ul>
						<?php foreach($classification as $class) : ?>
						<li>
							<a href="#">
							<?php echo $class->Classification; ?> &raquo;
							<ul class="multi-level">
								<?php foreach($job_titles as $job_title) : ?>
								<?php if($class->Classification == $job_title->Classification1) : ?>
								<li><a href="<?php echo base_url(); ?>detail/<?php echo protect_url($job_title->id); ?>"><?php echo $job_title->Classification2Name; ?></a></li>
								<?php endif; ?>
								<?php endforeach; ?>
							</ul>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
				</li>

				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/building.png">
						Area
						<span class="icon">&nbsp;</span>
					</a>
					<ul>
						<li><a href="<?php echo base_url(); ?>area/" <?php echo (!$this->session->userdata('area_search') || $this->session->userdata('area_search') == 'all') ? 'class="red"' : ''; ?>>All Areas</a></li>
						<?php foreach($area as $area_item) : ?>
						<li><a <?php echo ($this->session->userdata('area_search') == $area_item->CityID) ? 'class="red"' : '';?> href="<?php echo base_url(); ?>area/<?php echo protect_url($area_item->CityID); ?>"><?php echo $area_item->City; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</li>

				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/car.png">
						Location
						<?php if($this->session->userdata('area_search')) : ?>
						<span class="icon">&nbsp;</span>
						<?php endif; ?>
					</a>
					<?php if($this->session->userdata('area_search')) : ?>
					
					<ul>
						<li><a href="<?php echo base_url(); ?>area/subarea/" <?php echo (!$this->session->userdata('sub_area_search') || $this->session->userdata('sub_area_search') == 'all') ? 'class="red"' : ''; ?>>All Locations</a></li>
						<?php foreach($location as $loc) : ?>
						<li><a <?php echo ($this->session->userdata('sub_area_search') == $loc->SubCityID) ? 'class="red"' : '';?> href="<?php echo base_url(); ?>area/subarea/<?php echo protect_url($loc->SubCityID); ?>"><?php echo $loc->SubCity; ?></a></li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>

				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/List.png">
						Work Type
						<span class="icon">&nbsp;</span>
					</a>
					<ul>
						<li><a <?php echo (!$this->session->userdata('type_search') || $this->session->userdata('type_search') == 'all') ? 'class="red"' : ''; ?> href="<?php echo base_url(); ?>type/">All Work Type</a></li>
						<?php foreach($type as $type_item) : ?>
						<li><a <?php echo ($this->session->userdata('type_search') == $type_item->employmenttype_valueid) ? 'class="red"' : ''; ?> href="<?php echo base_url(); ?>type/<?php echo protect_url($type_item->employmenttype_valueid); ?>"><?php echo $type_item->employmenttype; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</li>	
				
					
				<li class="send_right"><a class="round_right" href="#">
					<img src="<?php echo base_url(); ?>assets/images/icons/grey/magnifying_glass.png">
					Search
					<span class="icon">&nbsp;</span></a>
					<div class="drop_box right round_all">
						<form style="width:210px" action="<?php echo base_url(); ?>search/" method="GET" name="search-option">
							<input class="round_all" value="" placeholder="Search..." name="keyword">
							<button class="send_right">Go</button>
						</form>
					</div>
				</li>				
				<li class="send_right">
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/Key.png">
						Login
						<span class="icon">&nbsp;</span>
					</a>
					<div class="drop_box right round_all">
						<form style="width:160px" action="" name="login" method="POST">
							<fieldset class="grid_8">
								<label>Email</label><input class="round_all" value="name@example.com">
							</fieldset>
							<fieldset class="grid_8">
								<label>Password</label><input class="round_all" type="password" value="password">
							</fieldset>
							<button class="send_right">Login</button>
						</form>
					</div>
				</li>				
			</ul>
		</div>
		
		