<?php $this->load->view('header'); ?>

<body <?php echo $background_setting; ?>>
	<div id="wrapper" class="container_16">
		
		<?php $this->load->view('menu'); ?>
		<?php $this->load->view('side_bar'); ?>
		
		<div id="main" class="grid_13 omega jobs_list">
			<div class="content round_all clearfix">
					<div class="clearfix">
						<div style="float:left;width:75%">
							<div>
								<h2 style="border-bottom:1px solid #6E7785" class="red"><b><?php echo $all; ?></b> jobs from <b><?php echo $advertiser; ?></b></h2>
							</div>
							
							<?php if(!empty($jobs)) : ?>
							<?php foreach($jobs as $job) : ?>
							<section class="clearfix job-wrapper">
								<div style="float:left;width:70%">
									<div class="job-header">
										<h4><a class="red" style="text-decoration:none" href="<?php echo base_url(); ?>detail/<?php echo protect_url($job->PrimaryKey); ?>"><?php echo $job->position; ?></a></h3>
										<h6><?php echo $job->advertisername; ?></h6>
										
									</div>
									<p>
									<?php
										$string = strip_tags($job->description);
										if (strlen($string) > 150) {
										// truncate string
										$stringCut = substr($string, 0, 150);
										// make sure it ends in a word so assassinate doesn't become ass...
										$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
										}
										echo $string; 
									?>
									</p>
									<span><?php echo $job->classification; ?> &raquo; <?php echo $job->employmenttype; ?></span>
									<div style="text-align:center;margin:5px 0">
										<a href="<?php echo base_url(); ?>apply/<?php echo protect_url($job->PrimaryKey); ?>"><img src="<?php echo base_url(); ?>assets/images/apply.png" alt="Apply Now" ></a>
										
										<a href="<?php echo base_url(); ?>detail/<?php echo protect_url($job->PrimaryKey); ?>"><img src="<?php echo base_url(); ?>assets/images/detail.png" alt="Job Detail"></a>
									</div>
								</div>
								<div style="float:right;width:20%;padding:20px;border-left:1px solid #DDDDDD;font-size:12px;">
									<p><?php echo $job->location; ?> &raquo; <?php echo $job->area; ?></p>
									
									<?php $sal_arr = explode('.',$job->salarycurrency);?>
									<?php if($job->salaryminimum != '') : ?>
									<p>Salary Max : <?php echo $sal_arr[1] ;?> <?php echo $job->salaryminimum; ?></p>
									<?php endif; ?>
									
									<?php if($job->salarymaximum != '') : ?>
									<p>Salary Max : <?php echo $sal_arr[1] ;?> <?php echo $job->salarymaximum; ?></p>
									<?php endif; ?>
									
									
								</div>
							</section>
							<?php endforeach; ?>
							<?php else :?>
							No jobs found.
							<?php endif; ?>
						</div>
						<div style="float:right;width:20%">
							<?php $this->load->view('google_ads'); ?>
						</div>
					</div>
					
					<nav class="page">
						<?php echo $page; ?>
					</nav>
			</div>
			
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
	
</body>
</html>