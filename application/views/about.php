<?php $this->load->view('header'); ?>
<?php $this->load->helper('protect_url'); ?>
<body <?php echo $background_setting; ?>>
	<div id="wrapper" class="container_16">
		
		<?php $this->load->view('menu'); ?>
		<?php $this->load->view('side_bar'); ?>
		
		<div id="main" class="grid_13 omega">
			<div class="content round_all clearfix">
	
				
				<p>About Page</p>
			
			
			</div>
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
	<script type="text/javascript">
		$('#colour_switcher a').click(function(){
			var colour = $(this).attr('id');
			var cssUrl = ('theme_'+colour+'.css');
			
			var link = $("<link>");
			link.attr({
			        type: 'text/css',
			        rel: 'stylesheet',
			        href: 'assets/styles/skins/'+cssUrl
					});
					
			$("head").append( link );
			
	        $("img").each(function(){
	              $(this).attr("src", $(this).attr("src").replace("grey", "white"));  
	        });
		});
		
		$('#bg_switcher a').click(function(){
			var link = $(this).attr('title');
			window.location='<?php echo base_url(); ?>setting/index/'+link;
			return false;
		});
		
		
	</script>
</body>
</html>