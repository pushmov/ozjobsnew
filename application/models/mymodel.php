<?php

Class Mymodel extends Appmodel
{

	public function getClassificationOptions()
	{
		$sql = "SELECT DISTINCT
			`classification` AS Classification1Name , 
			`classification_valueid` AS Classification1ID 
			FROM `xml_jobg8_oz` ORDER BY `classification`";
		return $this->db->query($sql)->result();
	}
	
	public function distinctClassification(){
		$sql = "SELECT DISTINCT 
			`classification` AS Classification
			FROM `xml_jobg8_oz` ORDER BY `classification`";
		return $this->db->query($sql)->result();
	}
	
	public function getSubClassificationOptions()
	{
		$sql = "SELECT DISTINCT 
			`position` AS Classification2ID , 
			`position` AS Classification2Name ,
			`PrimaryKey` AS id,
			`classification` AS Classification1,
			`classification_valueid`
			FROM `xml_jobg8_oz` ORDER BY `position`";
		return $this->db->query($sql)->result();
	}
	
	public function getLocationOptions()
	{	
		$sql = "SELECT DISTINCT
				`location` AS City , 
				`location_valueid` AS CityID 
			FROM `xml_jobg8_oz` ORDER BY `location`";
		return $this->db->query($sql)->result();
	}
	
	public function getAreaOptions($CityID)
	{
		if($CityID>0)
		{
			$params = array();
			$sql = "SELECT DISTINCT `area` AS SubCity, `area_valueid` AS SubCityID FROM `xml_jobg8_oz` WHERE `location_valueid`='$CityID' ORDER BY `area`";
			return $this->db->query($sql)->result();		
		}
		else
			return array();
	}
	
	public function getEmployersOptions()
	{
		$sql = "SELECT DISTINCT
				`advertisername` 
			FROM `xml_jobg8_oz` WHERE `advertisertype_valueid` IN (15892) ORDER BY `advertisername`";
		return $this->db->query($sql)->result();
	}
	
	public function getWorkTypeOptions()
	{
		$sql = "SELECT DISTINCT
				`employmenttype` , 
				`employmenttype_valueid` 
			FROM `xml_jobg8_oz` ORDER BY `employmenttype`";
		return $this->db->query($sql)->result();
	}
	
	public function getJobsListBySector($sectorid,$limit,$offset,$options){
		if($sectorid != 'all'){
		$this->db->where(array('classification_valueid' => $sectorid));
		}
		
		if(!empty($options)){
		$this->db->where(array($options['key'] => $options['value']));
		}
		$query = $this->db->get('xml_jobg8_oz',$limit,$offset);
		
		return $query->result();
	}
	
	public function getJobsListByArea($area,$limit,$offset,$options){
		if($area != 'all'){
		$this->db->where(array('location_valueid' => $area));
		}
		if(!empty($options)){
		$this->db->where(array($options['key'] => $options['value']));
		}
		$query = $this->db->get('xml_jobg8_oz',$limit,$offset);
		return $query->result();
	}
	
	public function getJobsListBySubArea($subarea,$limit,$offset){
		if($subarea != 'all'){
		$this->db->where(array('area_valueid' => $subarea));
		}
		
		$query = $this->db->get('xml_jobg8_oz',$limit,$offset);
		return $query->result();
	}
	
	public function getJobsListByType($typeid,$limit,$offset,$options){
		if($typeid != 'all'){
		$this->db->where(array('employmenttype_valueid' => $typeid));
		}
		if(!empty($options)){
		$this->db->where(array($options['key'] => $options['value']));
		}
		$query = $this->db->get('xml_jobg8_oz',$limit,$offset);
		return $query->result();
	}
	
	public function getJobsListByAdvertiser($ad_id,$limit,$offset){
		$this->db->where(array('advertisername' => $ad_id));
		$query = $this->db->get('xml_jobg8_oz',$limit,$offset);
		return $query->result();
	}
	
	public function distinctNav($where,$distinct){
		
		
		$where = (empty($where)) ? "" : " WHERE $where[key] = $where[value]";
	
		$sql = "SELECT DISTINCT
				$distinct
			FROM `xml_jobg8_oz` ". $where;
		return $this->db->query($sql)->result();
	}
	
	public function getLatestemployer()
	{
		$sql = "SELECT 
					a.`PrimaryKey` , 
					a.`position` , 
					a.`description` 
				FROM 
					`xml_jobg8_oz` a LIMIT 50 ";
		return $this->db->query($sql)->result();
	}	
	
	public function wildcard($key,$limit,$offset){
		
		return $this->db->like('description',$key)
			->or_like('position',$key)
			->get('xml_jobg8_oz',$limit,$offset)
			->result();
		// $sql = "SELECT *FROM 
					// `xml_jobg8_oz` WHERE description LIKE '%$key%' OR position LIKE '%$key%' LIMIT $limit, $offset";
		// return $this->db->query($sql)->result();
	}
	
	public function getLocationByParent($area_id){
		if($area_id != 'all'){
			$sql = "SELECT DISTINCT
				area,area_valueid
			FROM `xml_jobg8_oz` WHERE location_valueid = ". $area_id;
			return $this->db->query($sql)->result();
		} else return array();
		
		
	}
	
	
}