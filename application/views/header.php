<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" /> 

<?php if(isset($title)) : ?>
<title><?php echo $title; ?></title>
<?php else : ?>
<title>Oz Jobs Online for all Australian Jobs Online</title>
<?php endif; ?>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/skins/theme_blue.css"/> 
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/reset.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/text.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/960_fluid.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/main.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/bar_nav.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/side_nav.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/css3.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>	

<?php if(ENVIRONMENT == 'production') : ?>
<script language=JavaScript> var message="Our website is Copyrighted. Trying to see our source or steal images is illegal"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false") </script>
<?php endif; ?>

</head>
<script>
	$(document).ready(function(){
		
		runner();
	});		
</script>
<script>
	function runner()
		{	
			setTimeout("runner()",3000);
			scrolling();
		}
		function scrolling()
		{
			$('.scrolling').each(function(){
				var s = $(this);
				var c = $('li', s).length;
				if(c>3)
				{
				if($('li:hidden', s).length)
				{
					$('li:hidden', s).last().next().fadeOut("slow");
					if($('li:hidden', s).length>c-6)
					{
						var cp = $('li:hidden', s).first().clone();
						$('li:hidden', s).first().remove();
						cp.appendTo(s).fadeIn('slow');
					}
				}
				else
				{
					$('li', s).first().fadeOut("slow");
				}
				}
				
			});
		}
</script>