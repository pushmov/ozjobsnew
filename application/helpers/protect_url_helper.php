<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('protect_url'))
{
    function protect_url($plain_url = '')
    {
			return str_replace('%2F','-',urlencode(
			trim(
				base64_encode(
					mcrypt_encrypt(
						MCRYPT_RIJNDAEL_256, MY_SALT, $plain_url, MCRYPT_MODE_ECB, mcrypt_create_iv
							(mcrypt_get_iv_size
								(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))))));
    }
}