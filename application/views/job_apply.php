<?php $this->load->view('header'); ?>

<body <?php echo $background_setting; ?>>
	<div id="wrapper" class="container_16">
		
		<?php $this->load->view('menu'); ?>
		<?php $this->load->view('side_bar'); ?>
		
		<div id="main" class="grid_13 omega jobs_list">
			<div class="content round_all clearfix">
					<div class="clearfix">
						<div style="float:left;width:40%">
							<div>
								<h3 style="border-bottom:1px solid #6E7785">Advertiser : <span class="red"><?php echo $job->advertisername; ?></span></h3>
							</div>
						</div>
						<div style="float:right;width:56%">
							<div>
								<div class="clearfix" style="border-bottom:1px solid #6E7785;">
									<h6 style="float:left"><a class="red" style="text-decoration:none;font-size:11px;" href="<?php echo base_url(); ?>advertiser/search/<?php echo protect_url($job->advertisername); ?>">More jobs by this advertiser</a></h6>
									<h6 style="float:right"><a class="red" style="text-decoration:none;font-size:11px;" href="javascript:void(0)" onclick="window.history.go(-1)">&laquo; back to results</a></h6>
								</div>
							</div>
						</div>
					</div>
					
					<div>
						<h3 style="margin:15px 0;" class="red"><?php echo $job->position; ?></h3>
						<p><span class="grey">Work Type: </span><?php echo $job->employmenttype; ?></p>
						<p><span class="grey">Classification: </span><?php echo $job->classification; ?></p>
						<iframe height="470" frameborder="0" width="100%" scrolling="no" align="middle" border="0" cellspacing="0" cellpadding="0" src="<?php echo $job->applicationurl; ?>"></iframe>				
					</div>
					
			</div>
			
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
	
</body>
</html>