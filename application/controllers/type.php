<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Type extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index($type_id=NULL)
	{
		
		$options=array();
		
		if(isset($_GET['search'])){
			
			if(ctype_alpha(str_replace(' ','', urldecode($_GET['search'])))){
					
					$options['key'] = 'location';
					$options['value'] = $_GET['search'];
					
			} else {
					
					
					$options['key'] = 'classification_valueid';
					$options['value'] = $this->dec(urlencode($_GET['search']));
			}
			
		}
		
		
		$type_id = (isset($type_id)) ? $this->dec($type_id) : NULL;
		$type_id = (isset($type_id)) ? $type_id : 'all';
		$this->session->set_userdata('type_search',$type_id);
		$data['all'] = $this->Mymodel->getJobsListByType($type_id,NULL,NULL,$options);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'type/'.$this->enc($type_id).'/';
		$config['total_rows'] = count($data['all']);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$data = $this->_menu_data($data);
		
		$data['enable_nav_1'] = true;
		$data['enable_nav_2'] = true;
		$data['enable_nav_3'] = false;
		
		$data['enable_location_nav'] = false;
		
		$dist_nav = array();
		$inside_loop = array();
		if($type_id != 'all'){
			$dist_nav = array('key' => 'employmenttype_valueid', 'value' => $type_id);
			$inside_loop = array('employmenttype_valueid' => $type_id);
		}
		
		$smart_nav = $this->Mymodel->distinctNav($dist_nav, 'classification_valueid,classification');
		foreach($smart_nav as $nav){
			$nav->total = sizeof($this->Mymodel->fetch_rows(NULL,array_merge(array('classification_valueid' => $nav->classification_valueid), $inside_loop)));
		}
		
		$data['smart_nav_2'] = $smart_nav;
		
		
		$smart_nav_2 = $this->Mymodel->distinctNav($dist_nav, 'location_valueid, location');
		foreach($smart_nav_2 as $nav2){
			$nav2->total = sizeof($this->Mymodel->fetch_rows(NULL,array_merge(array('location_valueid' => $nav2->location_valueid), $inside_loop)));
		}
		$data['smart_nav_1'] = $smart_nav_2;
		
		$data['jobs'] = $this->Mymodel->getJobsListByType($type_id,ITEM_PER_PAGE,$this->uri->segment(3),$options);
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$data['change_sector'] = 'type/';
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('employmenttype_valueid' => $type_id));
		
		$data['category'] = (($type_id) != 'all') ? $row->employmenttype : 'All Type';
		
		$this->load->view('jobs_list',$data);
		
	}
}



