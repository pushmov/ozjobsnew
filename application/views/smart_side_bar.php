		<div id="side_nav" class="side_nav grid_3">
			<ul class="clearfix">
				<li><a class="round_left"><span class="red">Refine Search</span></a></li> 
				
				<?php if($show_left_side_categories) : ?>
				<li><a class="round_left" href="#"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Cog.png"><span class="red">Other Categories</span></a></li> 
				<?php foreach($left_side_categories_jobs as $cat) : ?>
				<li>
					<a href="<?php echo base_url(); ?>sector/?category=<?php echo $cat->classification_valueid; ?>">
						<?php if(trim($cat->classification) == 'Advert / Media / Entertainment') :?>
						<span>Advert / Media (<?php echo $cat->total; ?>)</span>
						<?php elseif(trim($cat->classification == 'I.T. & Communications')) : ?>
						<span>I.T &amp; Comm. (<?php echo $cat->total; ?>)</span>
						<?php else : ?>
						<span><?php echo $cat->classification; ?> (<?php echo $cat->total; ?>)</span>
						<?php endif; ?>
					</a>
				</li>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<?php if($enable_nav_1) : ?>
				<li><a class="round_left" href="#"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Cog.png"><span class="red">Sector</span></a></li> 
				<?php foreach($smart_nav_2 as $s2) : ?>
				<?php if(isset($change_sector)) : ?>
				<li>
					<a href="<?php echo base_url(); ?><?php echo 'type/'.protect_url($this->session->userdata('type_search')); ?>/?search=<?php echo protect_url($s2->classification_valueid); ?>">
						<?php if(trim($s2->classification) == 'Advert / Media / Entertainment') :?>
						<span>Advert / Media (<?php echo $s2->total; ?>)</span>
						<?php elseif(trim($s2->classification == 'I.T. & Communications')) : ?>
						<span>I.T &amp; Comm. (<?php echo $s2->total; ?>)</span>
						<?php else : ?>
						<span><?php echo $s2->classification; ?> (<?php echo $s2->total; ?>)</span>
						<?php endif; ?>
					</a>
				</li>
				<?php else : ?>
				<li>
					<a href="<?php echo base_url(); ?><?php echo 'area/'.protect_url($this->session->userdata('area_search')); ?>/?search=<?php echo protect_url($s2->classification_valueid); ?>">
						<?php if(trim($s2->classification) == 'Advert / Media / Entertainment') :?>
						<span>Advert / Media (<?php echo $s2->total; ?>)</span>
						<?php elseif(trim($s2->classification == 'I.T. & Communications')) : ?>
						<span>I.T &amp; Comm. (<?php echo $s2->total; ?>)</span>
						<?php else : ?>
						<span><?php echo $s2->classification; ?> (<?php echo $s2->total; ?>)</span>
						<?php endif; ?>
					</a>
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<?php if($enable_nav_2) : ?>
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/building.png">
						<span class="red">Area</span>
					</a>
				</li>
				
				<?php foreach($smart_nav_1 as $s1) : ?>
				<?php if(isset($change_sector)) : ?>
				<li>
					<a href="<?php echo base_url(); ?>type/<?php echo protect_url($this->session->userdata('type_search')); ?>/?search=<?php echo strtolower($s1->location); ?>">
						<?php if($s1->location == 'Australian Capital Territory') : ?>
						<span>Aust Capital Territory (<?php echo $s1->total; ?>)</span>
						<?php else : ?>
						<span><?php echo $s1->location; ?> (<?php echo $s1->total; ?>)</span>
						<?php endif; ?>
					</a>
				</li>
				<?php else : ?>
				<li>
					<a href="<?php echo base_url(); ?>sector/<?php echo protect_url($this->session->userdata('sector_search')); ?>/?search=<?php echo strtolower($s1->location); ?>">
						<?php if($s1->location == 'Australian Capital Territory') : ?>
						<span>Aust Capital Territory (<?php echo $s1->total; ?>)</span>
						<?php else : ?>
						<span><?php echo $s1->location; ?> (<?php echo $s1->total; ?>)</span>
						<?php endif; ?>
					</a>
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<!-- start optional filter -->
				
				
				<?php if($enable_location_nav) : ?>
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/car.png">
						<span class="red">Location</span>
					</a>
				</li>
				
				<?php if(isset($came_from_subarea)) : ?>
				<?php foreach($child_locations as $child):?>
				<li>
					<a href="<?php echo base_url(); ?>area/subarea/<?php echo protect_url($child->area_valueid); ?>/<?php echo (strlen($_SERVER['QUERY_STRING']) > 0) ? '?'.$_SERVER['QUERY_STRING'] : ''; ?>"><?php echo $child->area; ?></a>
				</li>
				<?php endforeach; ?>
				<?php else : ?>
				<?php foreach($child_locations as $child):?>
				<li>
					<a href="<?php echo base_url(); ?>area/<?php echo $hashed_areaid; ?>/?location=<?php echo $child->area; ?>"><?php echo $child->area; ?></a>
				</li>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<?php endif; ?>
				<!-- end optional filter -->

				<?php if($enable_nav_3) : ?>
				<form id="refine_form" name="refine_form" action="<?php echo base_url(); ?>welcome/ajax_request/<?php echo (strlen($_SERVER['QUERY_STRING']) > 0) ? '?'.$_SERVER['QUERY_STRING'] : ''; ?>">
				<li><a class="round_left" href="#"><img src="<?php echo base_url();?>assets/images/icons/grey/List.png"><span class="red">Work Type</span></a></li> 
				<input type="hidden" name="current" value="<?php echo $current; ?>">
				<input type="hidden" name="current_key" value="<?php echo $current_key; ?>">
				<input type="hidden" name="category" value="<?php echo $category; ?>">
				<input type="hidden" name="from" value="<?php echo $this->uri->segment(1); ?>">
				
				<li>
					<a>
					<input type="checkbox" name="type[]" class="ajax-call" value="any" checked="checked">
						<span>Any</span>
					</a>
				</li>
				<li>
					<a>
					<input type="checkbox" name="type[]" class="ajax-call" value="contract" checked="checked">
						<span>Contract</span>
					</a>
				</li>	
				<li>
					<a>
					<input type="checkbox" name="type[]" class="ajax-call" value="permanent" checked="checked" >
						<span>Permanent</span>
					</a>
				</li>	
				</form>
				<script>
					$(document).ready(function(){
						
						$('.ajax-call').attr('checked','checked');
						
						$('.ajax-call').click(function(){
							
							
								var val = $('input[name="type"]').val();
								var form = $('#refine_form');
								$.ajax({
									type: "POST",
									url: form.attr('action'),
									data: form.serialize(),
									success: function(response){
										
										$('#main').html(response);
									}
								});
							
							
						});
					});
				</script>
				
				
				<?php endif; ?>
				
			</ul>		
			
		</div>
		
		