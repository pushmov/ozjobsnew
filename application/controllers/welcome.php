<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index()
	{
		$data = array();
		$data = $this->_menu_data($data);
		
		
		$data['jobs'] = $this->Mymodel->getLatestemployer();
		
		$this->load->view('welcome_message',$data);
	}
	
	public function ajax_request(){
		
		$add_sql = '';
		
		if($this->input->post('from') == 'sector'){
			if(isset($_GET['search'])){
				$add_sql = "AND location = '$_GET[search]'";
				$this->session->set_userdata('ajax_location',$_GET['search']);
			}
		} elseif($this->input->post('from') == 'area'){
			if(isset($_GET['search'])){
				$class = $this->dec(urlencode($_GET['search']));
				$add_sql = "AND classification_valueid = '$class'";
				$this->session->set_userdata('ajax_classification',$class);
			}
		}
		
		if(isset($_GET['search'])){
			$data['title'] = ucwords($_GET['search']);
		} elseif(isset($_GET['location'])){
			$data['title'] = ucwords($_GET['location']);
		}
		
		
		if(isset($_GET['location'])){
			$location_child = $_GET['location'];
			if($location_child != ''){
				$add_sql .= "AND area = '$location_child'";
			}
		}
		
		
		$current = $this->input->post('current');
		$this->session->set_userdata('ajax_current',$current);
		if(empty($current)){
			return;
		}
		
		$session_key = $this->input->post('current_key');
		$this->session->set_userdata('ajax_current_key',$session_key);
		
		$session = $this->session->userdata($current);
		
		$add_sql1 = "AND $session_key = '$session'";
		if($session == 'all'){
			$add_sql1 = "";
		}
		
		$arr_type = array();
		foreach($this->input->post('type') as $type){
			$arr_type[] = (string) "'".$type."'";
		}
		$this->session->set_userdata('ajax_type',$arr_type);
		
		$sql_type = implode(',',$arr_type);
		
		$all = $this->db->query(
			"SELECT * FROM xml_jobg8_oz WHERE employmenttype IN ($sql_type) $add_sql1 $add_sql"
		)->result();
		$data['all'] = $all;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'welcome/ajax_request_nav/';
		$config['total_rows'] = count($all);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$result = $this->db->query(
			"SELECT * FROM xml_jobg8_oz WHERE employmenttype IN ($sql_type) $add_sql1 $add_sql LIMIT 0,".ITEM_PER_PAGE
		)->result();
		
		$this->session->set_userdata('ajax_category',$this->input->post('category'));
		
		$data['jobs'] = $result;
		$data['category'] = ($this->session->userdata('area_cat') == '') ? $this->input->post('category') : $this->session->userdata('area_cat');
		
		$this->load->view('ajax_jobs_list',$data);
	}
	
	public function ajax_request_nav($page=0){
		
		$add_sql = '';
		if(isset($_GET['search'])){
			$add_sql = "AND location = '$_GET[search]'";
			$this->session->set_userdata('ajax_location',$_GET['search']);
		}
		
		$current = $this->session->userdata('ajax_current');
		$this->session->set_userdata('ajax_current',$current);
		if(empty($current)){
			return;
		}
		
		$session_key = $this->session->userdata('ajax_current_key');
		$this->session->set_userdata('ajax_current_key',$session_key);
		
		$session = $this->session->userdata($current);
		
		$add_sql1 = "AND $session_key = '$session'";
		if($session == 'all'){
			$add_sql1 = "";
		}
		
		$arr_type = array();
		foreach($this->session->userdata('ajax_type') as $type){
			$arr_type[] = (string) $type;
		}
		
		$sql_type = implode(',',$arr_type);
		
		$all = $this->db->query(
			"SELECT * FROM xml_jobg8_oz WHERE employmenttype IN ($sql_type) $add_sql1 $add_sql"
		)->result();
		$data['all'] = $all;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'welcome/ajax_request_nav/';
		$config['total_rows'] = count($all);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$result = $this->db->query(
			"SELECT * FROM xml_jobg8_oz WHERE employmenttype IN ($sql_type) $add_sql1 $add_sql LIMIT $page,".ITEM_PER_PAGE
		)->result();
		
		
		$data['jobs'] = $result;
		$data['category'] = $this->session->userdata('ajax_category');
		$this->load->view('ajax_jobs_list',$data);
	}
	
	public function contact(){
		$data = array();
		$data = $this->_menu_data($data);
		$this->load->view('contact',$data);
	}
	
	public function about(){
		$data = array();
		$data = $this->_menu_data($data);
		$this->load->view('about',$data);
	}
	
	
	
}
