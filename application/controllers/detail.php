<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}

	public function index($job_id=NULL)
	{
		
		if(!isset($job_id)){
			redirect('/');
		}
		
		$job_id = $this->dec($job_id);
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$detail = $this->Mymodel->fetch_row(NULL,array('PrimaryKey' => $job_id));
		
		if(empty($detail)){
			redirect('/');
		}
		
		
		$data['detail'] = $detail;
		$data['title'] = $detail->position;
		$data['back'] = $this->session->userdata('uri_string');
		$data = $this->_menu_data($data);
		
		$this->load->view('job_detail',$data);
		
	}
}



