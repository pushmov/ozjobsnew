<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Share extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}
	
	public function index($id=NULL)
	{
		if(!isset($id)){
			redirect('/');
		}
		
		$id = $this->dec($id);
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('PrimaryKey' => $id));
		
		if(empty($row)){
			redirect('/');
		}
		
		$data = array();
		$data = $this->_menu_data($data);
		$data['job'] = $row;
		
		$this->load->view('job_share',$data);
	}
	
}
