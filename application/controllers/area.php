<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
	}
	
	public function index($area_id=NULL)
	{
		
		$options=array();
		
		$parent_area_name = '';
		
		$data['header_search_title'] = 'job(s) in All Areas';
		
		if(isset($area_id) && $this->dec($area_id) != 'all'){
			$this->Mymodel->set_table('xml_jobg8_oz');
			$get_area_name = $this->Mymodel->fetch_row(NULL,array('location_valueid' => $this->dec($area_id)));
			$data['header_search_title'] = 'job(s) in ' . $get_area_name->location;
		}
		$data['show_left_side_categories'] = false;
		
		if(isset($_GET['search'])){
			$options['key'] = 'classification_valueid';
			$options['value'] = $this->dec(urlencode($_GET['search']));
			$this->session->set_userdata('parent_area_name',$options['value']);
			
			if($options['value'] != 'all'){
				
				$get_name_cat = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => $options['value']));
				
				$this->session->set_userdata('area_cat',$get_name_cat->classification);
				
				if(isset($area_id) && $this->dec($area_id) != 'all'){
					$get_area_name = $this->Mymodel->fetch_row(NULL,array('location_valueid' => $this->dec($area_id)));
					$data['header_search_title'] = $get_name_cat->classification . ' job(s) in ' . $get_area_name->location;
				} else {
					$data['header_search_title'] = $get_name_cat->classification . ' job(s) in All Areas';
				}
			}
			
			
		}
		
		if(isset($_GET['location'])){
			$options['key'] = 'area';
			$options['value'] = urlencode($_GET['location']);
			
			$get_name_cat = $this->Mymodel->fetch_row(NULL,array('classification_valueid' => $this->session->userdata('parent_area_name')));
			$data['header_search_title'] = $get_name_cat->classification . ' job(s) in ' . $options['value'];
		}
		
		//UcdkPfypuvqcNjbbZpPlWe1tK8Rkj8dySC3Ud%2BMuYNA%3D
		//UcdkPfypuvqcNjbbZpPlWe1tK8Rkj8dySC3Ud%2BMuYNA%3D
		$data['hashed_areaid'] = $area_id;
		
		
		
		$area_id = (isset($area_id)) ? $this->dec($area_id) : NULL;
		$area_id = (isset($area_id)) ? $area_id : 'all';
		$this->session->set_userdata('area_search',$area_id);
		$data['all'] = $this->Mymodel->getJobsListByArea($area_id,NULL,NULL,$options);
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'area/'.$this->enc($area_id).'/';
		$config['total_rows'] = count($data['all']);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$data = $this->_menu_data($data);
		
		$data['enable_nav_1'] = true;
		$data['enable_nav_2'] = false;
		$data['enable_nav_3'] = true;
		
		/** show location option */
		$data['enable_location_nav'] = true;
		
		$dist_nav = array();
		$inside_loop = array();
		if($area_id != 'all'){
			$dist_nav = array('key' => 'location_valueid', 'value' => $area_id);
			$inside_loop = array('location_valueid' => $area_id);
		}
		
		$smart_nav = $this->Mymodel->distinctNav($dist_nav, 'classification_valueid,classification');
		foreach($smart_nav as $nav){
			$nav->total = sizeof($this->Mymodel->fetch_rows(NULL,array_merge(array('classification_valueid' => $nav->classification_valueid), $inside_loop)));
		}
		
		$data['smart_nav_2'] = $smart_nav;
		
		$data['jobs'] = $this->Mymodel->getJobsListByArea($area_id,ITEM_PER_PAGE,$this->uri->segment(3),$options);
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$data['current'] = 'area_search';
		$data['current_key'] = 'location_valueid';
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('location_valueid' => $area_id));
		
		
		$data['category'] = (($area_id) != 'all') ? $row->location : 'All Areas';
		
		
		/** fetch location based on area id */
		
		$locations = $this->Mymodel->getLocationByParent($area_id);
		$data['child_locations'] = $locations;
		
		$this->load->view('jobs_list',$data);
		
	}
	
	public function subarea($sub_area_id=NULL){
		
		$sub_area_id = $this->dec($sub_area_id);
		
		$area_id = $this->session->userdata('area_search');
		$sub_area_id = (isset($sub_area_id)) ? $sub_area_id : 'all';
		$this->session->set_userdata('sub_area_search',$sub_area_id);
		$data['all'] = $this->Mymodel->getJobsListBySubArea($sub_area_id,NULL,NULL);
		
		
		$locations = $this->Mymodel->getLocationByParent($area_id);
		$data['child_locations'] = $locations;
		$data['hashed_areaid'] = $area_id;
		$data['came_from_subarea'] = 'Y';
		
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('area_valueid' => $sub_area_id));
		$data['header_search_title'] = 'job(s) in '.$row->area;
		$data['show_left_side_categories'] = false;
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'area/'.$sub_area_id.'/';
		$config['total_rows'] = count($data['all']);
		$config['per_page'] = ITEM_PER_PAGE;
		$config['next_page'] = '&laquo;';
		$config['full_tag_open'] = '<ul>';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_page'] = '&raquo;';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$data = $this->_menu_data($data);
		
		$data['enable_nav_1'] = true;
		$data['enable_nav_2'] = false;
		$data['enable_nav_3'] = true;
		$data['enable_location_nav'] = true;
		
		$dist_nav = array();
		$inside_loop = array();
		if($sub_area_id != 'all'){
			$dist_nav = array('key' => 'area_valueid', 'value' => $sub_area_id);
			$inside_loop = array('area_valueid' => $sub_area_id);
		}
		
		$smart_nav = $this->Mymodel->distinctNav($dist_nav, 'classification_valueid,classification');
		foreach($smart_nav as $nav){
			$nav->total = sizeof($this->Mymodel->fetch_rows(NULL,array_merge(array('classification_valueid' => $nav->classification_valueid), $inside_loop)));
		}
		
		$data['smart_nav_2'] = $smart_nav;
		
		$data['jobs'] = $this->Mymodel->getJobsListBySubArea($sub_area_id,ITEM_PER_PAGE,$this->uri->segment(4));
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();
		
		$data['current'] = 'sub_area_search';
		$data['current_key'] = 'area_valueid';
		
		$this->Mymodel->set_table('xml_jobg8_oz');
		$row = $this->Mymodel->fetch_row(NULL,array('area_valueid' => $sub_area_id));
		
		
		$data['category'] = (($sub_area_id) != 'all') ? $row->area : 'All Locations';
		
		$this->load->view('jobs_list',$data);
	}
	
	
}
