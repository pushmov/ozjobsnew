<?php $this->load->view('header'); ?>
<?php $this->load->helper('protect_url'); ?>
<body <?php echo $background_setting; ?>>
	<div id="wrapper" class="container_16">
		
		<?php $this->load->view('menu'); ?>
		<?php $this->load->view('side_bar'); ?>
		
		<div id="main" class="grid_13 omega">
			<div class="content round_all clearfix">
				<div class=""><img src="<?php echo base_url(); ?>assets/images/logo.png"></div>
	
				
			<p><strong>Australian Jobs</strong> are easier to find on Oz Jobs Online. Looking for work in Australia? then you need to be finding work right here. Just chose from the many options
			that make it easier for you to find a job.</p>
			<p><strong>No sign ups</strong> are required to apply for a job. Our site is easier to use. Just search, find a job and apply - it's that simple</p>
			<p><strong>Post a Job</strong> for only $50.00 per job ad. Every job posted on our sites is instantly available across the More Jobs Online group of sites - thats worldwide with over 60 job boards</p>
			
			<p>If you don't have time to keep searching for a job now you can advertise for the jobs you want on Wanted Jobs Online for a small fee you can upload your resume and advertise for the 
			job that your looking for.</p>
			<br>
			
			<div style="margin:0 100px">
				<div style="float:left;width:223px;height:412px;overflow:hidden">
					<div id="column2" style="">
						<h3 style="color:#ff0000;font-size:13px">LATEST JOBS</h3>
						<p>
							
								<img width="220" border="0" height="110" style="margin-top: 6px;margin-bottom: 15px; border-style: solid; border-color: #999" src="<?php echo base_url(); ?>assets/images/latest_jobs.jpg"></a></p>   
								<ul class="scrolling">
								<?php foreach($jobs as $job) : ?>
									<li>
										<div id="itemheading" style="font-size:10px;"><?php echo $job->position; ?></div>
										<div id="newsitem" style="font-size:10px;color:#666666"><?php echo substr(strip_tags($job->description),0,100);?>.....
											<div id="more" style="text-align:right">
												<a class="applynow" style="text-decoration:none;color:#ff0000;font-size:10px;" href="<?php echo base_url(); ?>apply/<?php echo protect_url($job->PrimaryKey); ?>">Apply</a>&nbsp;&nbsp;&nbsp;&nbsp;
												<a target="7412824" class="addetail" style="text-decoration:none;color:#ff0000;font-size:10px;" href="<?php echo base_url(); ?>detail/<?php echo protect_url($job->PrimaryKey); ?>">View Job</a>
											</div>
										</div>
									</li>
								<?php endforeach; ?>
								</ul>            <!-- end #column2 -->
					</div>
				</div>
				<div style="float:right;width:223px;height:412px;overflow:hidden">
					<div id="column2" style="">
						<h3 style="color:#ff0000;font-size:13px">LATEST RECRUITER JOBS</h3>
						<p>
							<a href="">
								<img width="220" border="0" height="110" style="margin-top: 6px;margin-bottom: 15px; border-style: solid; border-color: #999" src="<?php echo base_url(); ?>assets/images/recruiting_jobs.jpg"></a></p>   
								<ul class="scrolling">
									<?php foreach($jobs as $job) : ?>
									<li>
										<div id="itemheading" style="font-size:10px;"><?php echo $job->position; ?></div>
										<div id="newsitem" style="font-size:10px;color:#666666"><?php echo substr(strip_tags($job->description),0,100);?>.....
											<div style="text-align:right">
												<a class="applynow" style="text-decoration:none;color:#ff0000;font-size:10px;" href="<?php echo base_url(); ?>apply/<?php echo protect_url($job->PrimaryKey); ?>">Apply</a>&nbsp;&nbsp;&nbsp;&nbsp;
												<a class="addetail" style="text-decoration:none;color:#ff0000;font-size:10px;" href="<?php echo base_url(); ?>detail/<?php echo protect_url($job->PrimaryKey); ?>">View Job</a>
											</div>
										</div>
									</li>
								<?php endforeach; ?>
								</ul>            <!-- end #column2 -->
					</div>
				</div>
			</div>
			<div style="margin-bottom:50px;clear:both"></div>
			
			<!--
			<p>Don't like our colours and backgrounds? well our site is customisable to the following colors, so just click on one of these colors to change the theme color.<p>
			<div id="colour_switcher" class="grid_16 alpha omega switcher">
					<h3>Colour Themes</h3>
					<a id="blue" href="#"><span>Blue</span></a>
					<a id="red" href="#"><span>Red</span></a>
					<a id="green" href="#"><span>Green</span></a>
					<a id="cyan" href="#"><span>Cyan</span></a>
					<a id="orange" href="#"><span>Orange</span></a>
					<a id="pink" href="#"><span>Pink</span></a>
					<a id="purple" href="#"><span>Purple</span></a>
					<a id="navy" href="#"><span>Navy</span></a>
					<a id="brown" href="#"><span>Brown</span></a>
			</div>
			<p>You can also chose a different background image by just clicking on any of the below images to change the background</p>
			
			<div id="bg_switcher" class="grid_16 alpha omega switcher">
					<h3>Background Images</h3>
					<a id="bg1" href="<?php echo base_url(); ?>assets/images/adelaide.jpg" title="Adelaide"></a>
					<a id="bg2" href="<?php echo base_url(); ?>assets/images/brisbane.jpg" title="Brisbane"></a>
					<a id="bg3" href="<?php echo base_url(); ?>assets/images/canberra.jpg" title="Canberra"></a>
					<a id="bg4" href="<?php echo base_url(); ?>assets/images/darwin.jpg" title="Darwin"></a>
					<a id="bg5" href="<?php echo base_url(); ?>assets/images/hobart.jpg" title="Hobart"></a>
					<a id="bg6" href="<?php echo base_url(); ?>assets/images/melbourne.jpg" title="Melbourne"></a>
					<a id="bg7" href="<?php echo base_url(); ?>assets/images/perth.jpg" title="Perth"></a>
					<a id="bg8" href="<?php echo base_url(); ?>assets/images/sydney.jpg" title="Sydney"></a>
					<a id="bg9" href="<?php echo base_url(); ?>assets/images/regional.jpg" title="Regional"></a>
			</div>
			-->
			</div>
		</div>
		<?php $this->load->view('footer'); ?>
	</div>
	<script type="text/javascript">
		$('#colour_switcher a').click(function(){
			var colour = $(this).attr('id');
			var cssUrl = ('theme_'+colour+'.css');
			
			var link = $("<link>");
			link.attr({
			        type: 'text/css',
			        rel: 'stylesheet',
			        href: 'assets/styles/skins/'+cssUrl
					});
					
			$("head").append( link );
			
	        $("img").each(function(){
	              $(this).attr("src", $(this).attr("src").replace("grey", "white"));  
	        });
		});
		
		$('#bg_switcher a').click(function(){
			var link = $(this).attr('title');
			window.location='<?php echo base_url(); ?>setting/index/'+link;
			return false;
		});
		
		
	</script>
</body>
</html>