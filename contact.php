<?php

/**
 * Class file to handle user requests for contact form
 * 
 * LICENSE:
 *
 * This source file is subject to the licensing terms that
 * is available through the world-wide-web at the following URI:
 * http://codecanyon.net/wiki/support/legal-terms/licensing-terms/.
 * 
 * PHP version >= 5.3
 *
 * @category  ContactForm
 * @package   ContactForm
 * @author    Kirti Kumar Nayak, India <thebestfreelancer.in@gmail.com>
 * @copyright 2013 TheBestFreelancer,
 * @license   http://codecanyon.net/wiki/support/legal-terms/licensing-terms/ CodeCanyon
 * @version   Release 1.9
 * @link      http://demos.thebestfreelancer.in/phpcontact/
 * @tutorial  http://demos.thebestfreelancer.in/phpcontact/documentation/
 */

/**
 * Check if Contact class exists or not and define if not
 */
if (!class_exists('Contact')) {
    
    /**
     * Class to handle contact form requests handled via url
     * 
     * This is a singleton pattern class and can be called via static methods
     * 
     * @category  ContactForm
     * @package   ContactForm
     * @author    Kirti Kumar Nayak, India <thebestfreelancer.in@gmail.com>
     * @copyright 2013 TheBestFreelancer,
     * @license   http://codecanyon.net/wiki/support/legal-terms/licensing-terms/ CodeCanyon
     * @version   Release 1.9
     * @link      http://demos.thebestfreelancer.in/phpcontact/
     * @tutorial  http://demos.thebestfreelancer.in/phpcontact/documentation/
     */
    class Contact
    {
        // {{{ properties
        
        /**
         * private variable for HTML string replaces
         * 
         * @access private
         * @var array The replaces for HTML string
         */
        private $_replaces;
        
        /**
         * private variable to store any string
         * mainly used to store user submitted data
         * 
         * @access private
         * @var string  The string submitted by user/visitor
         */
        private $_str;
        
        /**
         * private variable to store a string of allowable HTML tags
         * to be used against code / CSFR / XSS attacks by visitors/hackers
         * 
         * @access private
         * @var string  The allowable html tags for user
         */
        private $_allowedHTML;
        /**
         * private variable to store
         * an array of restricted HTML/special characters
         * to be used against code / CSFR / XSS attacks by visitors/hackers
         * 
         * @access private
         * @var string  The restricted special characters/strings for user
         */
        private $_restrictedChars;
        
        
        /**
         * private property to hold the lowercase letter set
         * to be used to generate random string
         * 
         * @access private
         * @var string  The lowercase character set string
         */
        private $_lowerCaseChars;
        
        /**
         * private property to hold the uppercase letter set
         * to be used to generate random string
         * 
         * @access private
         * @var string  The uppercase character set string
         */
        private $_upperCaseChars;
        
        /**
         * private property to hold the numeric character set
         * to be used to generate random string
         * 
         * @access private
         * @var string  The numeric character set string
         */
        private $_numericChars;
        
        /**
         * private property to hold the special character set
         * to be used to generate random string
         * 
         * @access private
         * @var string  The special character set string
         */
        private $_specialChars;
        
        /**
         * private variable for random characters used by captcha method
         * 
         * @access private
         * @var string  The character set from which captcha should be generated
         */
        private $_charSet;
        
        /**
         * Private variable to store captcha type
         * if you have no GD library installed you may switch over to
         * Javascript captcha
         * 
         * @access private
         * @var string  The type of the captcha wanted
         */
        private $_captchaType;
        
        /**
         * Private variable to store captcha width
         * 
         * @access private
         * @var int  The height of the captcha image
         */
        private $_captchaWidth;
        
        /**
         * Private variable to store the captcha height
         * 
         * @access private
         * @var int  The height of the captcha image
         */
        private $_captchaHeight;
        
        /**
         * Private variable to store the location of the font to be used in captcha image
         * 
         * @access private
         * @var string  The path string of the ttf font file
         */
        private $_captchaFontLocation;
        
        /**
         * Private variable to store the captcha image font size
         * 
         * @access private
         * @var float  The font size for the captcha image
         */
        private $_captchaFontSize;
        
        /**
         * Private variable to store the captcha string angle
         * 
         * @access private
         * @var float  The angle of the captcha string
         */
        private $_captchaCharAngle;
        
        /**
         * private variable to hold mail type
         * expected values 'plain' / 'html'
         * 
         * @access private
         * @var string  The string describing mail type
         */
        private $_mailType;
        
        /**
         * private variable to store plain template
         * 
         * @access private
         * @var string  The plain mail template string
         */
        private $_plainMailTemplate;
        
        /**
         * private variable to store html template
         * 
         * @access private
         * @var string  The html mail template string
         */
        private $_htmlMailTemplate;

        /**
         * private variable to store html reply template
         * 
         * @access private
         * @var string  The html reply mail template string
         */
        private $_replyHtmlMailTemplate;
        
        /**
         * private variable for page contents
         * 
         * @access private
         * @var string  The HTML page contents
         */
        private $_pageContents;
        
        /**
         * private variable for page output type option
         * possible values: fullHTML, embedded
         * 
         * @access private
         * @var string  The HTML page contents
         */
        private $_outputType;
        
        
        /**
         * private variable to store
         * the css class for contact form
         * 
         * @access private
         * @var string  The css class for contact form
         */
        private $_contactFormCss;
        
        /**
         * private variable to contain response and to be converted into json
         * 
         * @access private
         * @var mixed  Json data object for page responses
         */
        private $_response;
        
        /**
         * private variable to store contact e-mails
         * 
         * @access private
         * @var array  The E-Mail array to hold the recipients
         */
        private $_emails;
        
        /**
         * private variable to store system auto response mail id
         * 
         * @access private
         * @var string  The name and e-mail string of the system
         */
        private $_autoResponder;
        
        /**
         * private variable to store page compression option
         * 
         * @access private
         * @var bool  The option to enable / disable page compression
         */
        private $_pageCompression;
        
        /**
         * private static variable to hold class object
         * 
         * @access private
         * @staticvar
         * @var object  The current class object
         */
        private static $_classObject;
        
        // }}}
        // {{{ __construct()
        
        /**
         * Default constructor class to initialize variables and page data.
         * Accoring to singleton class costructor must be private
         * 
         * @return void
         * @access  private
         */
        private function __construct()
        {
            
            /*
             * set Error Reporting to all
             */
            error_reporting(E_ALL | E_STRICT);
            /*
             * Initialize a session if not started yet
             */
            if (session_id() === '') {
                @session_start();
            }
            /*
             * initialize the string replaces to be used in HTML script
             * to compress the HTML output
             */
            $this->_replaces		=	array("\n", "\r\n", "\r", "\t", '  ', '   ');
            
            /*
             * initialize the allowed html tags which user/visitor can
             * use to send a html formatted message
             * define more if you want
             */
            $this->_allowedHTML     =   '<a><br><div><p><span><strong>';
            $this->_allowedHTML     .=  '<h1><h2><h3><h4><h5><h6><hr>';
            $this->_allowedHTML     .=  '<table><tr><td><th><thead><tfoot>';
            
            /*
             * initialize the allowed html tags which user/visitor can
             * use to send a html formatted message
             * define more if you want
             */
            $this->_restrictedChars  =   array('"', 'javascript', '()', '\\');
            /*
             * Main user configurations start
             * You may edit as per your need from here
             * Please refer to documentation if you face problems
             * Or you may ask me in the support section
             */
            
            // captcha configurations
            
            /*
             * define captcha type as php
             * you may use javascript captcha too
             * possible values: php, js
             */
            
            $this->_captchaType     =   'php';
            
            /*
             * initialize captcha image width
             * it is defined as per the html design
             */
            $this->_captchaWidth     =   70;            
            /*
             * initialize the captcha image height
             * defined optimum for the design
             */
            $this->_captchaHeight    =   30;            
            /*
             * initialize the font file location to be used for captcha characters
             * it must be a valid ttf font file at the specified location
             */
            $this->_captchaFontLocation =   './MONOFONT.TTF';
            /*
             * initialize the font size of the captcha string
             * by default the maximum defined i.e. 80% of the image height
             */
            $this->_captchaFontSize  = $this->_captchaHeight * 0.8;
            /*
             * initialize the characters angle for the captcha
             * it is randomly set between -2 and 2
             * as the image height and font size are set
             */
            $this->_captchaCharAngle = rand(-2, 2);
            /*
             * initialize the lowercase character set from a-z
             */
            $this->_lowerCaseChars  =    'abcdefghijklmopqrstuvwxyz';
            /*
             * initialize the uppercase character set from A-Z
             */
            $this->_upperCaseChars  =    'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            /*
             * initalize the numeric character set from 0-9
             */
            $this->_numericChars    =    '0123456789';
            /*
             * initialize the special character set (add more or delete if you like)
             */
            $this->_specialChars    =    '!$%^&*+#~/|';
            /*
             * initialize the captcha characters as null
             */
            $this->_charSet		    =	 '';
            
            // mail configurations
            
            /*
             * Initialize mail type to catch user preferences
             * change it if you want plain mail witout any formatting
             */
            $this->_mailType        =    'html';
            /*
             * Initialize the e-mails so that it will be listed in a drop-down list
             * Please Change it according to your needs
             */
            $this->_emails = array(
                'Sales'             =>   'admin@thebestfreelancer.in',
                'Enquiry'           =>   'enquiry@thebestfreelancer.in',
                'Support'           =>   'support@thebestfreelancer.in'
            );
            /*
             * initiate auto reply system name and e-mail
             * Change it accodting to your needs
             */
            $this->_autoResponder   =   'noreply@morejobsonline.com';
            
            /*
             * Initialize plain mail content template
             */
            $this->_plainMailTemplate = '{userMessage}';
            $this->_plainMailTemplate .= "\r\n\r\n\r\n" . 'From';
            $this->_plainMailTemplate .= "\r\n" . 'Name : {userFullName}';
            $this->_plainMailTemplate .= "\r\n" . 'E-Mail : {userEmail}';
            // check for optional fields and add them if they are set
            if (isset($_POST['phone']) and (trim($_POST['phone']) !== '')) {
                $this->_plainMailTemplate .= "\r\n" . 'Phone : {userPhone}';
            }
            /*
             * define html mail content template
             */
            $this->_htmlMailTemplate = '<html><body>';
            $this->_htmlMailTemplate .= '<div style="margin:0 auto;padding:20px;width:80%;display:block;color:#000;background-color:#dddfea;font-family:Tahoma;border-radius:10px">';
            $this->_htmlMailTemplate .= '<div style="padding:5px;width:100%;display:block">';
            $this->_htmlMailTemplate .= '<p>{userMessage}</p>';
            $this->_htmlMailTemplate .= '</div>';
            $this->_htmlMailTemplate .= '<div style="margin-top:10px;padding-left:10px;font-weight:700;font-family:seriff;font-style:italic">';
            $this->_htmlMailTemplate .= '<h4>From</h4><hr />';
            $this->_htmlMailTemplate .= 'Name : {userFullName}<br />';
            $this->_htmlMailTemplate .= 'E-Mail : {userEmail}<br />';
            // check for optional fields and add them if they are set
            if (isset($_POST['phone']) and (trim($_POST['phone']) !== '')) {
                $this->_htmlMailTemplate .= 'Phone : {userPhone}<br />';
            }
            $this->_htmlMailTemplate .= '</div></body></html>';
            /*
             * define reply html mail content template
             */
            $this->_replyHtmlMailTemplate = '<html><body>';
            $this->_replyHtmlMailTemplate .= '<div style="margin:0 auto;padding:20px;width:100%;display:block;color:#000;font-family:Tahoma">';
            $this->_replyHtmlMailTemplate .= '<div style="padding:10px;width:80%;display:block">';
            $this->_replyHtmlMailTemplate .= 'Dear&nbsp;{userName}<br />';
            $this->_replyHtmlMailTemplate .= '<p>We just received your following mail.<br />We\'ll reach you as soon as possible.</p><br /><br />'; 
            $this->_replyHtmlMailTemplate .= '<i style="font-size:10px">This is an auto generated reply. Please <strong>do not reply to this e-mail</strong></i><br />';
            $this->_replyHtmlMailTemplate .= '<br /><div style="padding:15px;font-weight:normal;font-size:11px;border:#ccc 1px solid;border-radius:8px">';
            $this->_replyHtmlMailTemplate .= '<p>{userMessage}</p>';
            $this->_replyHtmlMailTemplate .= '</div></div>';
            $this->_replyHtmlMailTemplate .= '<div style="margin-top:10px;padding-left:10px;font-weight:700;font-family:seriff;font-style:italic">';
            $this->_replyHtmlMailTemplate .= '<h4>From</h4><hr />';
            $this->_replyHtmlMailTemplate .= $this->_autoResponder;
            $this->_replyHtmlMailTemplate .= '</div>';
            $this->_replyHtmlMailTemplate .= '</body></html>';
            
            // output configurations
            
            /*
             * set compression on to get gzipped contents
             * in php.ini.
             * You may set this off by commenting the
             * line if you have no zlib installed
             */
            @ini_set('zlib.output_compression', 'on');
            /*
             * initialize page contents as null
             */
            $this->_pageContents	= 	 '';
            /*
             * set page compression to true
             * you may set this false
             * if you don't like to compress the page contents
             */
            $this->_pageCompression =    true;
            /*
             * Initialize the response variable as null
             */
            $this->_response = array(
                'status'            =>   '',
                'message'           =>   '',
                'control'           =>   ''
            );
            
        }
        
        // }}}
        // {{{ getObject()
        
        /**
         * Method to return singleton class object.
         * returns current class object if already present
         * else creates one
         * 
         * @return object  The current class object
         * @access public
         * @static
         * 
         */
        public static function getObject()
        {
            /*
             *  check if class not instantiated
             */
            if (self::$_classObject === null) {
                /*
                 *  then create a new instance
                 */
                self::$_classObject = new self();
            }
            /*
             *  return the class object to be used
             */
            return self::$_classObject;
        }
        
        // }}}
        // {{{ _getRandomChars
        
        /**
         * Generate string of random characters
         *
         * @param int  $length         Length of the string to generate
         * @param bool $lowerCaseChars Include lower case characters
         * @param bool $upperCaseChars Include uppercase characters
         * @param bool $numericChars   Include numbers
         * @param bool $specialChars   Include special characters
         * 
         * @access private
         * @return string  The random character string
         */
        private function _getRandomChars (
            $length = 5,
            $lowerCaseChars = true,
            $upperCaseChars = true,
            $numericChars = true,
            $specialChars = false
        ) {
           
            /**
             * variable to store a random character index every time
             * @access private
             * @var int  The random character index out of character set
             */
            $charIndex               =    '';
           
            /**
             * variable to store a random character every time
             * @access private
             * @var char  The random character out of character set
             */
            $char                    =    '';
           
            /**
             * variable to store a random character set every time
             * @access private
             * @var int  The random character setof length 5 out of character set
             */
            $resultChars             =    '';
           
            /*
             * check if user has opted for lowercase characters
             * if true, then add it to the character set
             */
            if ($lowerCaseChars === true) {
                $this->_charSet      .=   $this->_lowerCaseChars;
            }
            /*
             * Check if user has opted for uppercase characters
             * If true, add it to the character set
             */
            if ($upperCaseChars === true) {
                $this->_charSet      .=   $this->_upperCaseChars;
            }
            /*
             * Check if user has opted for numeric characters
             * If true, add it to the character set
             */
            if ($numericChars === true) {
                $this->_charSet      .=   $this->_numericChars;
            }
            /*
             * Check if user has opted for uppercase characters
             * If true, add it to the character set
             */
            if ($specialChars === true) {
                $this->_charSet      .=   $this->_specialChars;
            }

            /*
             * Check if length has given greater than 0 else return null
             */
            if (($length < 0) || ($length == 0)) {
                return $resultChars;
            }

            /*
             * create a loop to get random 5 characters from the character set
             * 
             */
            for ($i = 0; $i < $length; $i++) {
                /*
                 * get the character randomly
                 * by selecting between 0 to length of the charSet
                 */
                $charIndex           =    rand(0, strlen($this->_charSet));
                $char                =    substr($this->_charSet, $charIndex, 1);
                $resultChars         .=   $char;
            }

            return $resultChars;
        }
        
        // }}}
        // {{{ _createPage()
        
        
        /**
         * Method to create page and output the HTML page string
         * in compressed form to be loaded faster
         *
         * @return mixed  Page data
         * @access private
         */
        private function _createPage()
        {
            /*
             * start output buffer to catch html output
             * @outputBuffering enabled
             */
            ob_start();
            /*
             * Now set HTML output
             * if Google map option is enabled
             * show that on left side,
             * with a space to provide address
             * of the organization
             */
           $this->_contactFormCss  =   'col-lg-offset-3 col-sm-offset-1 col-md-offset-2 col-lg-6 col-xs-12 col-sm-10 col-md-8';
            
            ?>
<!--
[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]
-->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
    *{
        font-family: Tahoma, sans-serif;
    }
    .contact-contents{
        margin: 10px auto;
        padding: 20px 10px 10px;
        border: 0;
        background-color: #fff;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -moz-box-shadow: 0px 0px 10px #111111; 
        -webkit-box-shadow: 0px 2px 10px #111111; 
        box-shadow: 0px 0px 10px #111111; 
    }
    .mandatory{
        color: #F00;
        font-weight: 800;
        font-size: 18px;
        margin-right: 2px;
    }
    .label {
        border-radius:0px !important;
    }
    #custom_map{
        height: 430px;
        display: block;
    }
    #leftcontents{
        margin-top: 10px;
    }
</style>

<!-- Main Container start -->
<div class="container">
    <!-- row start -->
    <div class="row">
        <a href="http://morejobsonline.com" target="_blank" title="Find More Jobs Online">
            <img src="assets/images/morejobs.jpg" alt="more jobs">
        </a>
        <a href="http://ozjobsonline.com" target="_blank" title="Find an Australian Jobs">Australian Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://americajobsonline.com" target="_blank" title="Find an American Jobs">American Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://canadasjobsonline.com" target="_blank" title="Find an Canadian Jobs">Canadian Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://asiansjobsonline.com" target="_blank" title="Find an Asian Jobs">Asian Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://africanjobsonline.com" target="_blank" title="Find an African Jobs">African Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://europesjobsonline.com" target="_blank" title="Find an European Jobs">European Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://britishjobsonline.com" target="_blank" title="Find an British Jobs">British Jobs</a>
        &nbsp;|&nbsp;
        <a href="http://nzjobsonline.com" target="_blank" title="Find an New Zealand Jobs">NZ Jobs</a>
    <!-- form start -->
    <form method="post" id="contact" action="<?php echo $_SERVER['PHP_SELF']; ?>?req=send" class="form-horizontal contact-contents" role="form">
        <!-- fieldset start -->
        <fieldset>
            <legend>Contact More Jobs Online</legend>
                <!-- div for error messages -->
                <div id="ErrorMsgs" class="col-md-12 col-sm-12 col-lg-12 col-xs-12" style="display:none"></div>
                <!-- /div for error messages end -->
                <!-- form group to hold controls -->
                <div class="form-group">
                    <label for="name" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>Name :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <input type="text" name="name" id="name" data-toggle="tooltip" placeholder="Enter your Full Name" class="form-control showhelp" title="Enter your Full name" />
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>
                <!-- /form group to hold controls end -->
                <div class="form-group">
                    <label for="email" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>E-mail :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <input type="email" name="email" id="email" data-toggle="tooltip" placeholder="yourname@domain.com" title="Enter your e-mail so that we can get in touch" class="form-control showhelp" />
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label">Phone :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <input type="tel" name="phone" id="phone" data-toggle="tooltip" placeholder="ex. 9816264666" class="form-control showhelp" title="Enter your phone or any contact number" />
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>

                <div class="form-group">
                    <label for="receiver" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>Send To :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <select name="receiver" id="receiver" data-toggle="tooltip" class="form-control showhelp" title="Select the department where you want to send the mail">
            <?php
            /*
             * Loop through the emails given and create a select box
             */
            foreach ($this->_emails as $name => $email):
            ?>
                            <option value="<?php echo $email; ?>"><?php echo $name; ?></option>
            <?php
            endforeach;
            ?>
                       </select>
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>
                
                <div class="form-group">
                    <label for="subject" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>Subject :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <input type="text" name="subject" id="subject" data-toggle="tooltip" placeholder="Enter the purpose" title="Enter your subject here" class="form-control showhelp" />
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>
                
                <div class="form-group">
                    <label for="message" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>Your Message :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-11">
                        <textarea name="message" id="message" class="form-control showhelp" data-toggle="tooltip" title="Your message goes here" placeholder="Enter Message"></textarea>
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>
                
                <div class="form-group">
                    <label for="captcha" class="col-md-4 col-sm-4 col-lg-4 col-xs-12 control-label"><span class="mandatory">*</span>Verify Captcha :</label>
                    <div class="col-md-7 col-sm-7 col-lg-7 col-xs-12">
                            <a href="Javascript:void(0);" id="capImg" class="showhelp col-md-3 col-sm-3 col-lg-3 col-xs-12" style="padding-left:0" data-toggle="tooltip" title="Click to get new challenge">
                                
                            </a>
                            <div class="col-md-9 col-sm-9 col-lg-9 col-xs-11" style="padding-right:0">
                                <input type="text" name="captcha" id="captcha" class="form-control pull-right showhelp" title="Just Verify the captcha" data-toggle="tooltip" placeholder="Verify captcha shown at left" />
                            </div>
                            
                    </div>
                    <div class="col-md-1 col-sm-1 col-lg-1 col-xs-1"></div>
                </div>
                
                <div class="form-group">
                    <label for="acknowledge" title="Check it if you want to recieve a reciept" class="showhelp col-md-offset-3 col-lg-offset-4 col-sm-offset-1 col-md-7 col-sm-9 col-lg-4 col-xs-12 control-label checkbox" data-toggle="tooltip">
                        <input type="checkbox" name="acknowledge" id="acknowledge" value="1" />
                        Acknowledge me with a mail receipt
                    </label>
                </div>
                
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-sm-offset-4 col-sm-8 col-lg-8 col-lg-offset-4 col-xs-12">
                      <button type="submit" class="btn btn-primary" id="submit" name="submit">Send Message</button>
                    </div>
                </div>
        </fieldset>
    </form>
    </div>
</div><!-- /end .row -->
</div><!-- /end #container -->
<footer>
    <p class="text-center">
        &COPY; <?php echo date('Y'); ?> <a href="http://morejobsonline.com/">More Jobs Online</a>
        &nbsp;|&nbsp;
        <a href="http://globologic.com">Globo Logic</a>
        
    </p>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
        
        /*
         * enable tooltip for specified class
         */
        $('.showhelp').tooltip();
        /*
         * load the captcha on page load
         * after checking user specified captcha type
         */
        $('#capImg').load('<?php echo $_SERVER['PHP_SELF']; ?>?req=captchaimg');
        
        /*
         * reload the captcha image on click the image
         * due to some browser issues of caching, a timestamp is appended to the url
         * it is most important and will not work for firefox or I.E. if removed
         */
        $('#capImg').click(function(){
            var date = new Date;
            $('#capImg').load('<?php echo $_SERVER['PHP_SELF']; ?>?req=captchaimg&tm='+date.getMilliseconds());
            return false;
        });
        /*
         * real time validation for name
         */
        $('#name').blur(function(){
            if($.trim($(this).val()) === ''){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        /*
         * real time validation for e-mail
         */
        $('#email').blur(function(){
            if(($.trim($(this).val()) === '') || (!$(this).val().match(/^([a-zA-Z0-9_.])+@([a-zA-Z0-9_.-])+.([a-zA-Z0-9]{2,6})$/))){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        /*
         * real time validation for phone
         */
        $('#phone').blur(function(){
            if(($.trim($(this).val()) === '') || (!$(this).val().match(/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/))){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        
        /*
         * real time validation for receiver
         */
        $('#receiver').blur(function(){
            if($.trim($(this).val()) === ''){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        /*
         * real time validation for subject
         */
        $('#subject').blur(function(){
            if($.trim($(this).val()) === ''){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        /*
         * real time validation for message
         */
        $('#message').blur(function(){
            if($.trim($(this).val()) === ''){
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').addClass('has-error');
            }else{
                $(this).parent().next('.col-md-1').html('<span class="glyphicon glyphicon-ok" style="color:#0F0;padding-top:7px"></span>');
                $(this).parent().parent('div.form-group').removeClass('has-error');
            }
            return false;
        });
        
        /*
         * validate the form when sumbitted
         */
        $('#submit').click(function(e){
            e.preventDefault();
            var submitUrl = $('#contact').attr('action');
            $.ajax({
                url: submitUrl,
                type: 'POST',
                data: $('#contact').serialize(),
                dataType: "json",
                beforeSend: function () {
                    $('#submit').attr('disabled', 'disabled');
                    if($(".alert").length > 0){
                        $(".alert").each(function(index, element) {
                            $(element).remove();
                        });
                    }
                    if($(".glyphicon").length > 0){
                        $(".glyphicon").each(function(index, element) {
                            $(element).remove();
                        });
                    }
                    if($(".form-group").length > 0){
                        $(".form-group").each(function(index, element) {
                            $(element).removeClass('has-error');
                        });
                    }
                    $('#ErrorMsgs').fadeOut('slow').html('<div class="alert alert-info">Checking...<a href="#" class="close">&times;</a></div>').fadeIn('slow');
                },
                success: function(data) {
                    $('#ErrorMsgs').html(data.message).fadeIn('slow');
                    var date = new Date;
                    if (data.status === 'success') {
                        $('#contact')[0].reset();
                        $('#capImg').load('<?php echo $_SERVER['PHP_SELF']; ?>?req=captchaimg&tm='+date.getMilliseconds());
                        $('#submit').removeAttr('disabled');
                    }else{
                        if(data.control==='captcha'){
                            $('#capImg').load('<?php echo $_SERVER['PHP_SELF']; ?>?req=captchaimg&tm='+date.getMilliseconds());
                            $('#'+data.control).parent().parent().parent('div.form-group').addClass('has-error');
                            $('#'+data.control).parent().parent().next('div.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                        }else{
                            $('#'+data.control).parent().parent('div.form-group').addClass('has-error');
                            $('#'+data.control).parent().next('div.col-md-1').html('<span class="glyphicon glyphicon-remove" style="color:#F00;padding-top:7px"></span>');
                        }
                        $('#submit').removeAttr('disabled');
                        $('#'+data.control).focus();
                    }
                }
            });
            return false;
        });
    });
</script>

            <?php
            /*
             * get the page contents
             */
            $this->_pageContents = ob_get_contents();
            /*
             * clean the buffer
             */
            ob_end_clean();
            
            /*
             * compress the page contents if page compression is on
             * else get the general output
             * 
             * CAUTION: while using page compression on,
             * re-check every elements and strings
             * as every whitespace character replaced without single space
             * i.e. Tabs(\t), line feeds(\n, \r, \r\n), double spaces, triple spaces etc.
             * are removed. Hence if you plan to use this, double check your html.
             */
            if ($this->_pageCompression) {
                $this->_pageContents = str_replace(
                    $this->_replaces,
                    '',
                    $this->_pageContents
                );
            }
            /*
             * return the contents string
             */
            return $this->_pageContents;
        }
        
        // }}}
        // {{{ _createPage()
        
        /**
         * Method to respond to the requests via url
         * 
         * @param bool   $pageCompression Option for the output data to be compressed or not
         * @param string $captchaType     The type of captcha wanted, possible values: php, js
         * @param string $mailType        The type of mail to be sent, possible values: html, text
         * @param string $emails          The address of mail to send, possible values: as specified
         * @param string $autoResponder   The address of reply mail to be sent back to the visitor
         * 
         * @return mixed  May output page HTML string or JSON validation data
         * @access public
         */
        public function respondRequest(
            $pageCompression,
            $captchaType,
            $mailType,
            $emails,
            $autoResponder
        ) {
            /*
             * assign the map options as defined
             * if true, then shows the map on the left side
             * and shifts the form to right side,
             * else shows only the form at center
             */
            
            /*
             * assign captcha type as user has defined
             * you may use javascript captcha too
             * possible values: php, js
             */
            $this->_captchaType     =   $captchaType;
            
            /*
             * assign mail type to catch user preferences
             * change it if you want plain mail witout any formatting
             */
            $this->_mailType        =    $mailType;
            /*
             * set page compression to user defined
             * you may set this false
             * if you don't like to compress the page contents
             */
            $this->_pageCompression =    $pageCompression;
            
            /*
             * set page compression to user defined
             * you may set this false
             * if you don't like to compress the page contents
             */
            $this->_emails          =    $emails;
            
            /*
             * set page compression to user defined
             * you may set this false
             * if you don't like to compress the page contents
             */
            $this->_autoResponder   =    $autoResponder;
            
            /*
             * catch the get variable
             * if set then act accordingly
             */
            if (isset($_GET['req'])) {
                /*
                 * switch over the request and respond accordingly
                 */
                switch ($_GET['req']) {
                case 'captcha':
                    /*
                     * call the method to create the captcha image
                     */
                    $this->createCaptcha();
                    break;
                case 'captchaimg':
                    /*
                     * check if user has opted php captcha and GD library present
                     * if user has them both then
                     * call the method to create the captcha image
                     * else just return the captcha characters
                     */
                    
                    if (extension_loaded('gd') and ($this->_captchaType==='php')) {
                        /*
                         * return an image tag
                         */
                        echo '<img src="'.$_SERVER['PHP_SELF'].'?req=captcha&tm='.time().'" alt="Captcha Image" title="Click to get new challenge" />';
                    } else {
                        /*
                         * take two variables with random integer values,
                         * then add them and save in session to verify
                         * @var int  Integers to work as captcha
                         */
                        $a          =   rand(1, 9);
                        $b          =   rand(1, 9);
                        /*
                         * Assign the characters to a session variable
                         */
                        session_regenerate_id();
                        $_SESSION['CaptchaChars']	=	$a+$b;
                        /*
                         * Close the session write buffer to avoid overwriting
                         */
                        session_write_close();
                        /*
                         * simply output the characters only
                         */
                        echo '<h4>'.$a.' + '.$b.' =</h4>';
                    }
                    break;
                case 'send':
                    /*
                     * call the method to validate and send the message
                     */
                    $this->sendMail();
                    break;
                default:
                    /*
                     * call the method to create the page and output the contents
                     */
                    $this->getPage();
                    break;
                }
                /*
                 * validate the name must not be empty
                 * and send json data
                 */
            } else {
                /*
                 * call the method to create the page and output the contents
                 */
                $this->getPage();
            }
        }
        
        // }}}
        // {{{ createCaptcha()
        
        /**
         * Method to create captcha image for bot verification
         *
         * @return mixed  Image for captcha
         * @throws Exception  GD or general exceptions
         * @access public
         */
        public function createCaptcha()
        {
            try {
                /*
                 * Assign the characters to a session variable
                 */
                $_SESSION['CaptchaChars']	=	$this->_getRandomChars(5, false, true, true, false);
                /*
                 * Close the session write buffer to avoid overwriting
                 */
                session_write_close();
                

                /*
                 * Create a 100 X 30 image and assign it to a var
                 */
                $img				=	 imagecreatetruecolor($this->_captchaWidth, $this->_captchaHeight);
                /*
                 * create a white color
                 */
                $white              =    imagecolorallocate($img, 255, 255, 255);
                
                /*
                 * Create a black color to write the characters prominently
                 */
                $black              =    imagecolorallocate($img, 0, 0, 0);
                /*
                 * fill the rectangular image with white background
                 */
                imagefilledrectangle($img, 0, 0, 399, 30, $white);
                
                /*
                 * Write the string inside the image
                 * with black color
                 */
                imagettftext(
                    $img,
                    $this->_captchaFontSize,
                    $this->_captchaCharAngle,
                    2,
                    25,
                    $black,
                    $this->_captchaFontLocation,
                    $_SESSION['CaptchaChars']
                );
                /*
                 * generating dots randomly in background
                 * to make an image noise
                 * if you want more noise replace the argument 5
                 * as per your requirement
                 */ 
                for ( $i=0; $i<5; $i++ ) {
                    imagefilledellipse(
                        $img,
                        mt_rand(0,  $this->_captchaWidth),
                        mt_rand(0,  $this->_captchaHeight),
                        2,
                        3,
                        0
                    );
                }
                
                /*
                 * generating lines randomly in background of image
                 * for more noise
                 * if you want more noise replace the argument 10
                 * as per your requirement
                 */ 
                for ( $i=0; $i<10; $i++ ) {
                    imageline(
                        $img,
                        mt_rand(0, $this->_captchaWidth),
                        mt_rand(0, $this->_captchaHeight),
                        mt_rand(0, $this->_captchaWidth),
                        mt_rand(0, $this->_captchaHeight),
                        0
                    );
                }
                /*
                 * Output the image
                 */
                header('Content-Type: image/gif');
                /*
                 * output a gif image
                 */
                imagegif($img);
                /*
                 * destroy the image to save server space
                 */
                imagedestroy($img);
            }
            catch(Exception $ex) {
                die('Oh no.. Something gone wrong... Details: ' . $ex->getMessage());
            }
        }
        
        // }}}
        // {{{ getPage()
        
        /**
         * Method to get page output contents
         * by getting the page contents and compressing them
         *
         * @return string  Page contents HTML string output
         * @access public
         */
        public function getPage()
        {
            /*
             * call the method to create and output the page contents
             */
            echo $this->_createPage();
        }
        
        // }}}
        // {{{ _cleanSubmittedData()
        
        /**
         * The following method makes a variable safe
         * as that may contain unacceptable formats or data
         * to prevent security holes those may be a threat
         * 
         * @param mixed $submittedData The data submitted by the user to be filtered
         * 
         * @return mixed  Cleaned data submitted by the user
         * @access protected
         */

        protected function cleanSubmittedData($submittedData)
        {
            try {
                /*
                 * check if the data is an array or not
                 */
                if (!is_array($submittedData)) {
                    /*
                     * if that is not an array, treat that as a string
                     */
                    $this->_str       =   $submittedData;
                     /*
                      * trim the spaces if any
                      */
                    $this->_str       =   trim($this->_str);

                    /*
                     * check if magic quotes are on or not
                     * if on then it must have inserted slashes before quotes and slashes
                     */
                    if (get_magic_quotes_gpc()) {
                        /*
                         * if magic quotes are on, it inserts a slash before any quotes, hence remove them
                         */
                        $this->_str   =   stripslashes($this->_str);
                    }
                    
                    /*
                     * escape the data and insert null where restricted characters found
                     */
                    $this->_str       =   str_ireplace($this->_restrictedChars, "", $this->_str);

                    /*
                     * allow the tags for user and strip off rest of them
                     */
                    $this->_str     =   strip_tags($this->_str, $this->_allowedHTML);
                    /*
                     * now return the cleaned data
                     */
                    return $this->_str;

                } else {
                    /**
                     * var to keep cleaned data array for a temporary period
                     * so that they can be returned in cleaned state
                     * and acceptable format
                     * 
                     * @var mixed  The injection cleaned data array
                     * @access private
                     */
                    $cleanArr       =   array();
                    /*
                     * if the data is an array
                     * fetch the array values one by one by the loop
                     */
                    foreach ($submittedData as $pointer=>$str) {
                        /*
                         * Recursively call clean function if the data is array
                         */
                        $cleanArr[$pointer]=$this->cleanSubmittedData($str);
                    }
                    /*
                     * return the cleaned data array
                     */
                    return $cleanArr;
                }
            }
            catch (Exception $ex) {
                /*
                 * Catch any Exceptions occured
                 */
                die('There seems an error while cleaning user submitted data. Description: '. $ex->getMessage());
            }
        }
        
        // }}}
        // {{{ sendMail()
        
        /**
         * Method to send normal mail
         * 
         * @return string  JSON data object for success or failure
         * @access public
         */
        public function sendMail()
        {
            /*
             * clean up the user submitted data with the defined method
             */
            $submittedData          =   $this->cleanSubmittedData($_POST);
            /*
             * validate the form data
             */
            if (!isset($submittedData['name']) or ($submittedData['name'] === '')) {
                /*
                 * fill out the response array variable
                 * with alert/error message having bootstrap styles
                 */
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter your Name';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'name';
                /*
                 * now send a json header
                 */
                header("Content-Type: application/json");
                /*
                 * output the json data by converting the response array into a json format
                 */
                echo json_encode($this->_response);
                /*
                 * exit the script
                 */
                exit(0);
            }
            
            /*
             * validate email via php predefined (inbuilt) function
             */
            if (!isset($submittedData['email']) or (strlen(filter_var($submittedData['email'], FILTER_VALIDATE_EMAIL)) < 1)) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter your Valid E-Mail';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'email';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            
            /*
             * validate email via php predefined (inbuilt) function
             */
            if (!isset($submittedData['email']) or (strlen(filter_var($submittedData['email'], FILTER_VALIDATE_EMAIL)) < 1)) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter your Valid E-Mail';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'email';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            /*
             * validate the phone number correct or not if entered
             */
            if (isset($submittedData['phone']) and !empty($submittedData['phone']) and (!preg_match('/^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/', $submittedData['phone']))) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter a correct phone number';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'phone';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            
            /*
             * validate subject for empty value
             */
            if (!isset($submittedData['subject']) or ($submittedData['subject']==='')) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter a Subject';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'subject';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            /*
             * validate the message empty or blank
             */
            if (!isset($submittedData['message']) or ($submittedData['message'] === '')) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter your Message';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'message';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            if (!isset($submittedData['captcha']) or !isset($_SESSION['CaptchaChars']) or ($submittedData['captcha'] != $_SESSION['CaptchaChars'])) {
                $this->_response['status']	=	'error';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Please Enter The Captcha Correctly';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'captcha';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
            /*
             * End up the output buffer
             * to decrease CPU load
             */
            @ob_end_clean();
            /*
             * required for IE, otherwise Content-Disposition may be ignored if any
             */
            if (ini_get('zlib.output_compression')) {
                @ini_set('zlib.output_compression', 'Off');
            }
            /*
             * Build up the mail and
             * Set headers first, else the mail may be caught as spam
             */
            $headers			=	array();
            $headers[]			=	"MIME-Version: 1.0";
            // set content type according to the option supplied
            if ($this->_mailType === 'html') {
                $headers[]			=	"Content-type: text/html; charset=iso-8859-1";
            } else {
                $headers[]			=	"Content-type: text/plain; charset=iso-8859-1";
            }
            $headers[]			=	"From: {$submittedData['name']} <{$submittedData['email']}>";
            $headers[]			=	"Reply-To: {$submittedData['name']} <{$submittedData['email']}>";
            $headers[]			=	"Subject: {$submittedData['subject']}";
            /*
             * Set final headers by separating them by newlines into a single string
             */
            $finalHeaderString		=	implode("\r\n", $headers);
            /*
             * Set up the message according to the template
             */
            if ($this->_mailType === 'html') {
                // set up the html template and add contents
                $message			= str_replace(
                    array(
                        '{userMessage}',
                        '{userFullName}',
                        '{userEmail}',
                        '{userPhone}'
                        ),
                    array(
                        nl2br($submittedData['message']),
                        $submittedData['name'],
                        $submittedData['email'],
                        $submittedData['phone']
                        ),
                    "{$this->_htmlMailTemplate}"
                );
                
            } else {
                // set up the plain mail template
                $message			= str_replace(
                    array(
                        '{userMessage}',
                        '{userFullName}',
                        '{userEmail}',
                        '{userPhone}'
                        ),
                    array(
                        $submittedData['message'],
                        $submittedData['name'],
                        $submittedData['email'],
                        $submittedData['phone']
                        ),
                    "{$this->_plainMailTemplate}"
                );
            }
            
            /*
             * Mail it and catch the result to further check if mail has sent or not
             */
            $mailed			=	mail($submittedData['receiver'], $submittedData['subject'], $message, $finalHeaderString);
            /*
             * Now check if the mailing was successful
             */
            if ($mailed) {
                /*
                 * if sender has checked the checkbox to get acknowledgement,
                 * then send a confirmation mail
                 */
                if (isset($submittedData['acknowledge']) and ($submittedData['acknowledge']==='1')) {
                    
                    // Build up the mail and
                    $headers			=	array();
                    $headers[]		=	"MIME-Version: 1.0";
                    $headers[]		=	"Content-type: text/html; charset=iso-8859-1";
                    $headers[]		=	"From: ".$this->_autoResponder;
                    $headers[]		=	"Reply-To: {$submittedData['receiver']}";
                    $headers[]		=	"Subject: reply: {$submittedData['subject']}";
                    $finalHeaderString	=	implode("\r\n", $headers);
                    // set up the html reply mail template
                    $message		= str_replace(
                        array(
                            '{userName}',
                            '{userMessage}'
                            ),
                        array(
                            $submittedData['name'],
                            nl2br($submittedData['message'])
                            ),
                        $this->_replyHtmlMailTemplate
                    );
                    /*
                     * Mail it and catch the result to further check if mail has sent or not
                     */
                    $mail			=	mail($submittedData['email'], 'reply: ' . $submittedData['subject'], $message, $finalHeaderString);
                }
                
                $this->_response['status']	=	'success';
                $this->_response['message']	=	'<div class="alert alert-success alert-dismissable">';
                $this->_response['message']	.=	'Your message has been sent';
                // if acknowledgement opted, then output a message to view inbox
                if (isset($submittedData['acknowledge']) and ($submittedData['acknowledge']==='1')) {
                    $this->_response['message']	.=	'.<br />Please check your acknowledgement in your inbox/spam';
                }
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'name';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            } else {
                /*
                 * Else give Out an error message and reset
                 */
                $this->_response['status']	=	'success';
                $this->_response['message']	=	'<div class="alert alert-danger alert-dismissable">';
                $this->_response['message']	.=	'Some Error Occured !';
                $this->_response['message']	.=	'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
                $this->_response['control']	=	'name';
                header("Content-Type: application/json");
                echo json_encode($this->_response);
                exit(0);
            }
        }
        
        // }}}
        // {{{ __clone()
        
        /**
         * According to singleton pattern instance, cloning is prihibited
         *
         * @return string  A message that states, cloning is prohibited
         * @access public
         */
        private function __clone()
        {
            /*
             * only set an error message
             */
            die('Cloning is prohibited for singleton instance.');
        }
        
        // }}}
        
    }
}

/**
 * Set the e-mails in an array so that it will be listed in a drop-down list
 * and to be passed in the function call by arguments.
 * Please Change it according to your needs
 * 
 * @var mixed  The emails array to be shown in select box for visitor
 */
$emails = array(
   'Sales'                          =>   'jasond@globologic.com',
   'Enquiry'                        =>   'enquiry@morejobsonline.com',
   'Support'                        =>   'support@morejobsonline.com'
);
/**
 * Set auto reply system name and e-mail
 * from where the acknowledgement to be sent.
 * Generally it is set to be the id where no reply to be sent
 * Change it according to your needs
 * 
 * @var string  The e-mail for auto reply system
 */
$autoResponder                      =   'noreply@morejobsonline.com';
/**
 * Set page compression option
 * Possible options boolean (true,false)
 * Change according to needs
 * 
 * @var bool  The page compression option
 */
$pageCompression                    =   true;

/**
 * Set desired captcha type
 * Possible values: php/js
 * Change according to needs
 * 
 * @var string  The type of captcha you want to display
 */
$captchaType                        =   'js';
/**
 * Set desired type of mail to send
 * Possible values: html/text
 * Change according to need
 * 
 * @var string  The type of mail to be sent
 */
$mailType                           =   'html';

/**
 * set your preferred time zone
 * else you will get datetime warning at the place of
 * copyright year
 */
date_default_timezone_set('Asia/Kolkata');

/**
 * finally call the class method to do the rest
 * with set options
 */
Contact::getObject()->respondRequest(
    $pageCompression,
    $captchaType,
    $mailType,
    $emails,
    $autoResponder
);
?>