		<div id="side_nav" class="side_nav grid_3">
			<ul class="clearfix">
				<li><a class="round_left" ><img src="<?php echo base_url(); ?>assets/images/icons/grey/admin_user.png"><span>Search Jobs by</span></a></li> 
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/settings_2.png">
						<span>Aussie Cities</span>
						<span class="icon">&nbsp;</span>
					</a>
					<ul>
						<li><a href="http://adelaidejobsonline.com" title="Find a job in Adelaide">Adelaide</a></li>
						<li><a href="http://brisbanejobsonline.com" title="Find a job in Brisbane">Brisbane</a></li>
						<li><a href="http://canberrajobsonline.com" title="Find a job in Canberra">Canberra</a></li>
						<li><a href="http://darwinjobsonline.com" title="Find a job in Darwin">Darwin</a></li>
						<li><a href="http://hobartjobsonline.com" title="Find a job in Hobart">Hobart</a></li>
						<li><a href="http://melbournejobsonline.com" title="Find a job in Melbourne">Melbourne</a></li>
						<li><a href="http://perthjobsonline.com" title="Find a job in Perth">Perth</a></li>
						<li><a href="http://sydneyjobsonline.com" title="Find a job in Sydney">Sydney</a></li>
					</ul>
				</li>

				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/settings_2.png">
						<span>Aussie States</span>
						<span class="icon">&nbsp;</span>
					</a>
				
					<ul>
						<li><a href="http://actjobsonline.com.au" title="Find a job in ACT">ACT</a></li>
						<li><a href="http://nswjobsonline.com.au" title="Find a job in NSW">NSW</a></li>
						<li><a href="http://ntjobsonline.com.au" title="Find a job in NT">NT</a></li>
						<li><a href="http://qldjobsonline.com.au" title="Find a job in QLD">QLD</a></li>
						<li><a href="http://sajobsonline.com.au" title="Find a job in SA">SA</a></li>
						<li><a href="http://tasjobsonline.com.au" title="Find a job in TAS">TAS</a></li>
						<li><a href="http://vicjobsonline.com.au" title="Find a job in VIC">VIC</a></li>
						<li><a href="http://wajobsonline.com.au" title="Find a job in WA">WA</a></li>
					</ul>
				</li>
		
				<li>
					<a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/settings_2.png">
						<span>Categories</span>
						<span class="icon">&nbsp;</span>
					</a>

					<ul>
						<li><a>A-K <span class="icon">&nbsp;</span></a>	
							<ul>
								<li><a href="http://accountantjobsonline.com">Accounting</a></li>
								<li><a href="#">Administration</a>	</li>
								<li><a href="#">Community</a>	</li>
								<li><a href="#">Construction</a>	</li>
								<li><a href="#">Consulting</a>	</li>
								<li><a href="#">Customer Service</a>	</li>
								<li><a href="#">Education</a>	</li>
								<li><a href="#">Engineering</a>	</li>
								<li><a href="#">Entertainment</a>	</li>
								<li><a href="#">Executive</a>	</li>
								<li><a href="#">Financial</a>	</li>
								<li><a href="#">Government</a>	</li>
								<li><a href="#">Hospitality</a>	</li>
								<li><a href="#">Insurance</a>	</li>
								<li><a href="#">IT</a>	</li>
							</ul>
						</li>

						<li><a>L-Z</a><span class="icon">&nbsp;</span></a>	
							<ul>
								<li><a href="http://legaljobsonline.com">Legal</a>	</li>
								<li><a href="#">Manufacturing</a>	</li>
								<li><a href="#">Medical</a>	</li>
								<li><a href="#">Mining</a>	</li>
								<li><a href="#">Primary Industry</a>	</li>
								<li><a href="#">Real Estate</a></li>
								<li><a href="#">Recruitment</a></li>
								<li><a href="#">Retail</a></li>
								<li><a href="#">Sales & Marketing</a></li>
								<li><a href="#">Science</a></li>
								<li><a href="#">Self Employ</a></li>
								<li><a href="#">Trades & Services</a>	</li>
								<li><a href="#">Transport Logistics</a></li>								
							</ul>
						</li>
					
					</ul>
				</li>		
			</ul>		
			<br>	
			<ul class="clearfix">
				<li><a class="round_left" href="#"><img src="<?php echo base_url(); ?>assets/images/icons/grey/admin_user.png"><span>Countries</span></a></li> 	
				<li><a href="http://africanjobsonline.com" title="Find a job in Africa"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Truck.png"><span>Africa</span></a></li>	
				<li><a href="http://asiansjobsonline.com" title="Find a job in Asia"><img src="<?php echo base_url(); ?>assets/images/icons/grey/T-Shirt.png"><span>Asia</span></a></li>
				<li><a href="http://canadasjobsonline.com" title="Find a job in Canada"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Suitcase.png"><span>Canada</span></a></li>	
				<li><a href="http://europesjobsonline.com" title="Find a job in Europe"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Tag.png"><span>Europe</span></a></li>	
				<li><a href="http://nzjobsonline.com" title="Find a job in New Zealand"><img src="<?php echo base_url(); ?>assets/images/icons/grey/shopping_bag.png"><span>New Zealand</span></a></li>	
				<li><a href="http://britishjobsonline.com" title="Find a job in UJ"><img src="<?php echo base_url(); ?>assets/images/icons/grey/running_man.png"><span>UK</span></a></li>	
				<li><a href="http://americajobsonline.com" title="Find a job in USA"><img src="<?php echo base_url(); ?>assets/images/icons/grey/post_card.png"><span>USA</span></a></li>							
			</ul>
		</div>