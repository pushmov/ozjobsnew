<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mymodel');
		$this->session->set_userdata('uri_string',uri_string());
	}
	
	protected function _common_data($data){
		return $data;
	}
	
	protected function _menu_data($data){
	
		$this->Mymodel->set_table('xml_jobg8_oz');
		$data['job_sectors'] = $this->Mymodel->getClassificationOptions();
		$data['classification'] = $this->Mymodel->distinctClassification();
		$data['job_titles'] = $this->Mymodel->getSubClassificationOptions();
		$data['area'] = $this->Mymodel->getLocationOptions();
		$data['location'] = $this->Mymodel->getAreaOptions($this->session->userdata('area_search'));
		
		$data['type'] = $this->Mymodel->getWorkTypeOptions();
		
		if($this->session->userdata('background_setting')){
			$data['background_setting'] = $this->session->userdata('background_setting');
		} else {
			$data['background_setting'] = 'style="background: url('.base_url().'assets/images/austflag.jpg) repeat scroll 0% 0% transparent;"';
		}
		
		
		return $data;
	}
	
	protected function enc($text){
		return str_replace('%2F','-',urlencode(
			trim(
				base64_encode(
					mcrypt_encrypt(
						MCRYPT_RIJNDAEL_256, MY_SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv
							(mcrypt_get_iv_size
								(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))))));
  }

  protected function dec($text) {
		return trim(
			mcrypt_decrypt(
				MCRYPT_RIJNDAEL_256, MY_SALT, base64_decode(
					urldecode(str_replace('-','%2F',$text))), MCRYPT_MODE_ECB, mcrypt_create_iv(
						mcrypt_get_iv_size(
							MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), 
								MCRYPT_RAND)));
  }

}