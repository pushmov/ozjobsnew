		<div id="footer_wrapper">
			<div id="footer" class="nav_up bar_nav round_all clearfix">
				<ul class="round_all clearfix">

					<li>
						<a class="round_left" href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/images/icons/grey/admin_user.png">
							Home
						</a>
					</li>
					<li><a href="<?php echo base_url(); ?>postjob.html"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Book.png">Post a Job</a></li>
					<li><a href="<?php echo base_url(); ?>contact/"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Phone.png">Contact Us</a></li>
					<li><a href="http://jobstoemail.com" title="Receive a job alert to your email"><img src="<?php echo base_url(); ?>assets/images/icons/grey/Mail.png">Job Alert</a></li>
					<li><a href="<?php echo base_url(); ?>about/" title="About Oz Jobs Online"><img src="<?php echo base_url(); ?>assets/images/icons/grey/info_about.png">About Us</a></li>
					<li class="send_right">
						<a class="round_right" href="#">
							<img src="<?php echo base_url(); ?>assets/images/icons/grey/magnifying_glass.png">
							Search
							<span class="icon">&nbsp;</span></a>
							<div class="drop_box right round_all">
								<form style="width:210px" action="<?php echo base_url(); ?>search/" method="GET" name="search-option">
									<input class="round_all" value="" name="keyword" placeholder="Search...">
									<button class="send_right">Go</button>
								</form>
							</div>
					</li>

					<li class="send_right"><a href="#">
						<img src="<?php echo base_url(); ?>assets/images/icons/grey/Key.png">
						Login
						<span class="icon">&nbsp;</span></a>
						<div class="drop_box right round_all">
							<form style="width:160px">
								<fieldset class="grid_8">
									<label>Email</label><input class="round_all" value="name@example.com">
								</fieldset>
								<fieldset class="grid_8">
									<label>Password</label><input class="round_all" type="password" value="password">
								</fieldset>
								<button class="send_right">Login</button>
							</form>
						</div>
					</li>

				</ul>
			</div>
		</div>